# Docker
I will follow the tutorial by [TechWorld with Nana](https://www.youtube.com/watch?v=3c-iBn73dDE&t=169s) in Youtube
* In Docker Hub I can find several official images from many apps
* Containers come in layers. So it starts with a layer with Linux, then a layer of dependencies, etc. This is good because only layers that need to be updated will be downloaded again
* **image**: package, app with dependecies.
* **container**: an image that is running
* **Tag**: version of an image
* Several containers can have the same port, because I can bind the port of a container with a local port, so I then use the bound local port to access the port of a container


## Commands
* `docker images`: lists the images I have pulled
* `docker ps`: what containers are running
* `docker run`: equals to `docker pull` + `docker start`: runs a container (add `-d` to run in dettached mode)
* `docker run -p local_port:container_port`: `-p`: bind the container port to a local port
* `docker run --name someName imageName`: I can customize the name of the container, so it ie easier to call later
* `docker exec -it container_it /bin/bash`: to get the bash prompt inside a container
* `docker stop container_id`: stops a running container (id can be found with `docker ps`)
* `docker ps -a`: list also containers that are not running
* `docker logs container_id`: see logs of container
* `docker rm cont_id`: delete container

## Docker network
* Docker creates an isolated network. So containers can talk to each other just by using their names. If I want a local process to talk to a docker container, I have to use localhost:port.
* I should create a docker network for the containers that are going to talk to each other: `docker network create nameOfNetwork`


## Demo project
The code for this project comes from the authors [GitLab repository](https://gitlab.com/nanuchi/techworld-js-docker-demo-app)

1. Pull: mongo, mongo-express (a UI for mongo)
2. Run mongo
3. Run mongo-express, and connect it to mongo
4. Create network: `docker network create mongo-network`
5. Start mongo container:
```bash
docker run -p 27017:27017 \ # specify ports
-d \ # run in detached mode
-e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password \ # set env variables
 --name mongodb \ # set nane
 --net mongo-network \ # set network
 mongo # name of image
 ```
 6. Start mongo-express and connect to mongo
 ```bash
 docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password \
 -e ME_CONFIG_MONGODB_SERVER=mongodb \ # this is the name of the container running mongo (from 5.)
 --net mongo-network --name mongo-express mongo-express
 ```
 7. Go to `localhost:8081` to see the mongo UI
 8. Create "user-account" database from UI
 9. Inside "user-account", create a collection "users"

 ### Docker compose
 * To save initialization commands to a series of containers into a config file (docker-compose.yaml)
 * Structure
 ```yaml
 version: "3"              # version of docker compose
 services:                # containers
    mongodb:              # name of container
        image: mongo      # name of image
        ports:
            - 27017:27017 # local:container port binding
        environment:      # environmental variables
            - MONGO_INITDB_ROOT_USERNAME=admin
            - MONGO_INITDB_ROOT_PASSWORD=password
```
* I dont have to create a network, because all the containers in the same docker-compose.yaml will be in the same network
* `docker-compose -f docker-compose.yaml up`: start all the containers
* Look into `wait` or `depends_on` to check which one works better to wait for one container to load before starting another container

### Docker file
* Name it `Dockerfile`
```Dockerfile
# base image
FROM node
# environment variables. It is better to set the environment in docker-compose
ENV ...
# linux commands to run (starts in the root of the container)
RUN mkdir /dataFolder
# copy executes from local to the container
COPY . /dataFolder
# command to start the app. this has to be the entry point of the ap
CMD ["node", "server.js"]
```
* Make an image: `docker build -t my-app:1.0 /path/to/Dockerfile`. Here, my-app is the name of the image, and 1.0 is the tag assigned to it
* I have to re-build an image if I change the docker file

## Private Docker repos
* In AWS, it is called "ECR"
* Image naming: `registryDomain/imageName:tag`

## Data persistance - Volumes
Map a container path to a local path, so the data in the container remains  
Types (from `docker run`)
* `-v local/path:/container/path`: specifies paths in both
* `-v /container/path`: specifies path on container (anonymous)
* `-v name:/container/path` specifies a name and the path in the container.
In docker-compose. Here, I define where to mount named containers in each one, and also in the root level, I list all the volumes I used. I can even mount the same named volume in several containers.  
The `driver: local` is so that docker creates the physical folder in the local system. Locally, Docker stores the volume data at `/var/lib/docker/volumes`. When creating anonymous volumes, the local folder is a hash
```yaml
services:
    mongodb:
        ...
        volumes:
            - db-data:/path/to/data/in/container
    another-cont:
        ...
        volumes:
            - db-data:/path/in/the/other/container
volumes:
    db-data:
        driver: local
```