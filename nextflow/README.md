# NextFlow
I will be following the official [documentation](https://www.nextflow.io/docs/latest/getstarted.html)

## Getting started
1. Installation: `curl -s https://get.nextflow.io | bash`
2. Mark `nextflow` executable

## How it works
* When modifying a pipeline, only steps that were edited are run again
* When specifying `params.some_var`, I can run the pipeline with `nextflow run pipeline.nf --some_var algo`, and `params.some_var` will get the value `algo`
* A channel is a queue from one process that can provide data to another step. In the following pipeline, the process `splitLetters` specifies the channel `letters` that is grabbed by the input of `converetToUpper`
```
params.str = 'Hello world!'

process splitLetters {

    output:
    file 'chunk_*' into letters

    """
    printf '${params.str}' | split -b 6 - chunk_
    """
}


process convertToUpper {

    input:
    file x from letters.flatten()

    output:
    stdout result

    """
    #cat $x | tr '[a-z]' '[A-Z]'
    rev $x
    """
}

result.view { it.trim() }
```
* After writing a pipeline, I can run it on several systems (HPC, AWS, etc) by only changing the target execution system

## Nextflow scripting
From the [docs](https://www.nextflow.io/docs/latest/script.html#script-page)
* Print: `println "Hello world"`
* booleans are lowercase
* Length of list: `someList.size()`
* Maps (dictionaries): `scores = [ "Brett":100, "Pete":"Did not finish", "Andrew":86.87934 ]`
* Multiple assignment is possible: `(a, b, c) = [10, 20, 'foo']`
* Conditionals
```groovy
x = Math.random()
if( x < 0.5 ) {
    println "You lost."
}
else {
    println "You won!"
}
```
* String interpolation
```groovy
foxtype = 'quick'
foxcolor = ['b', 'r', 'o', 'w', 'n']
println "The $foxtype ${foxcolor.join()} fox"
```
* Nextflow comes with a number of variables accessible from the global scope of a pipeline. See reference [here](https://www.nextflow.io/docs/latest/script.html#implicit-variables)
* From those, `baseDir`, `launchDir`, and `rojectDir` are defined in the nextflow config file
* Each process (step) can use a number of variables (`disk`, `cpus`, `memory`). They are accessible in the script part by using the prefix `task` (e.g. `$task.cpus`)
## Closures
A closure is code that I can pass as an argument to a function by encapsulating the expression in curly braces. Collect is like map, and will apply the function square to each element of the array
```groovy
square = {x * x}
[1,2,3].collect(square)
```
* Closures can also accept more thn one argument. Here passing a map will pass each key/value pair to the closure
```groovy
printMapClosure = {
    key, value ->
    println "$key = $value"
}

[ "Yue" : "Wu", "Mark" : "Williams", "Sudha" : "Kumari" ].each(printMapClosure)
```
## Regex
Reference found [here](https://www.nextflow.io/docs/latest/script.html#regular-expressions)

## Files
In order to work with files, I put the path string into `file()`. When using wildcards, the file ethod returns a list holding the paths that match the glob expression
* Operations with files
```groovy
myFile = file("some/path/to/file.txt")
// write to file
myFile.text = "string of text"
// append to file
myFile.append("an extra line")
// or
myFile << "one more line"
```
* Bynary works similarly, but I use `myFile.bytes`
* These methods put all the content of the file in memory at once. If the files are big, it is better to read them one line at the time.
* The following does put the entire file in memory as an array, so I can iterate over it
```groovy
// read
myFile = file('some/my_file.txt')
allLines  = myFile.readLines()
for( line : allLines ) {
    println line
}
// or
file('some/my_file.txt')
    .readLines()
    .each { println it }
```
* In order to read files one line at the time:
```groovy
count = 0
myFile.eachLine {  str ->
        println "line ${count++}: $str"
    }
```
* More read/write functions, with things similar to contexts in python can be found [here](https://www.nextflow.io/docs/latest/script.html#advanced-file-reading-operations)
* To iterate over the files in a directory
```groovy
allFiles = myDir.list()
for( def file : allFiles ) {
    println file
}
```
* I can also do `myDir.listFiles()`. The difference is that `.list()` returns a list of strings, and `listFiles()` returns a list of file objects
* Create directories: `myDir.mkdir()`. It returns `true` if created successfully
* To create parent directories if necessary, use `myDir.mkdirs()`
* Copy files: `myFile.copyTo("dest/ina/tion")`. It behaves similar to `cp -r`
* Counting
    * lines: `myFile.countLines()`
    * Fasta records: `myFile.countFasta()`
    * FastQ records: `myFile.countFastq()`


## Processes
* A process is a step in a pipeline
* Anatomy of a process
```
process < name > {

   [ directives ]

   input:
    < process inputs >

   output:
    < process outputs >

   when:
    < condition >

   [script|shell|exec]:
   < user script to be executed >

}
```
### Script
* a string statement that defines the command to be run
* Each process contains only one script block, and it should be the last block of a process
* To access system environment varibles, use triple double-quotes, and escape the $ (e.g. `\$PATH`). If I use the `shell` block definition instead of `script`, I dont have to escape the $ of system variables
* I can write code there directly:
```sh
process pyStuff {

    """
    #!/usr/bin/python

    x = 'Hello'
    y = 'world!'
    print(f"{x} {y}"
    """

}
```
* I can run different code snippets conditionally. For example, if the user wants to use different aligners:
```
seq_to_align = ...
mode = 'tcoffee'

process align {
    input:
    file seq_to_aln from sequences

    script:
    if( mode == 'tcoffee' )
        """
        t_coffee -in $seq_to_aln > out_file
        """

    else if( mode == 'mafft' )
        """
        mafft --anysymbol --parttree --quiet $seq_to_aln > out_file
        """

    else if( mode == 'clustalo' )
        """
        clustalo -i $seq_to_aln -o out_file
        """

    else
        error "Invalid alignment mode: ${mode}"

}
```
* I can also put the code in their own files. The files should be located in a `templates` directory, which should be in the same directory as the nextflow pipeline
```
process template_example {

    input:
    val STR from 'this', 'that'

    script:
    template 'my_script.sh'

}
```
* When using the `shell` block, $ is used for system variables, and ! for nexflow variables (e.g. `echo User $USER says !{str}`). When using this, the block should be delimited by triple single-quotes
I am [here](https://www.nextflow.io/docs/latest/process.html)

### Input
* Inputs are "from" channels that receive data from a channel to be processed
* Syntax:
```
input:
    <input qualifier> <input name> [from <source channel>] [attributes]
```
* **input qualifier**: Declares the type of data to be received [table](https://www.nextflow.io/docs/latest/process.html#inputs)
* **input name**: name of the channel to be created.
* **[from `<source channel>`]**: Channel name that sends data to the input. If the source channel name has the same name as the input name, this block can be omitted
* I can make the pipeline use a fixed name, like naming a set of fastas to be blasted as `query.fa`. For example with this: `file query_file name 'query.fa' from proteins`, I can invoke the command `"blastp -query query.fa -db nr"` for all my files, and each file will be staged with the name `query.fa`
* Input types `file` and `path` are very similar, but `path` is recommended, because it converts strings to file objects
* The type `stdin` allows the values of the channel to be treated as standard input of the process
* The type `tuple` allows me to send tuple of values, like `[(1, "first option"), (2, "second...")]`
* The type `each` allows me to run a for loop inside a process. Here, in each step, three alignment methods will be run for each fasta
```
sequences = Channel.fromPath('*.fa')
methods = ['regular', 'expresso', 'psicoffee']

process alignSequences {
  input:
  file seq from sequences
  each mode from methods

  """
  t_coffee -in $seq -mode $mode > result
  """
}
```
* I can use severl input channels. If `val` is used, the process terminates when the first array ends
```
process foo {
  echo true
  input:
  val x from Channel.from(1,2)
  val y from Channel.from('a','b','c')
  script:
   """
   echo $x and $y
   """
}
```
* If I use queue channels (`from`) and `value` channels, the process will run until the queue channel ends (double check this)
```
process bar {
  echo true
  input:
  val x from Channel.value(1)
  val y from Channel.from('a','b','c')
  script:
   """
   echo $x and $y
   """
}
```

### Output
* Output channels is where the data is sent when it has been processed
* Syntax
```
output:
    <output qualifier> <output name> [into <target channel>[,channel,..]] [attribute [,..]]

```
* **Qualifier**: data type
* **output name**: name of channel
* **into**: Channel the output will send data to. This can be several channels
* When the output name is the same as the channel name, the `into` part can be omitted
* When the outut file name has `*` or `?`, it is interpreted as a glob. This is useful to output several files per process. However, with this, I will end up with a list of lists (one list per input, with several files each). In order to flatten that list, I can use `file 'chunk_*' into letters mode flatten`.
* In Nextflow, the output files dont have to be tied to input (like in snakemake). This wont cause problems, because each step is done in a different folder, so I can set the output as the same file name. However, I can dynamically create output names by using `file "${x}.aln" into genomes`, where `x` comes from the input
* Similar to `stdin`, the `stdout` type can be used to pipe the output from each step in a process through the channel
* The `env` output type allows me to send things through the channel as environment variables that can be captured by another channel

### When
I can set conditionals to decide if a process is to be run. It takes any boolean expression

```groovy
process find {
  input:
  file proteins
  val type from dbtype

  when:
  proteins.name =~ /^BB11.*/ && type == 'nr'

  script:
  """
  blastp -query $proteins -db nr
  """

}
```

### Directives
These are extra settings to a process, like a conda environment, use of GPUs, etc. Reference [here](https://www.nextflow.io/docs/latest/process.html#directives). Directives should be set right before the body of the process.
* Example for using conda. Here, packages can be specified with spaces. I can also set the conda channel used to install the packages. Instead of setting the apps names directly, I can use `conda path/to/requirements.yaml`. I can also use the `conda` directive to use existing conda environments by `conda /path/to/conda/env/dir`
```groovy
process foo {
  conda 'bioconda::bwa=0.7.15 bioconda::fastqc=0.11.5'

  '''
  your_command --here
  '''
}
```
* Similarly, this is how I run with a container. This does not work with AWS batch or Kubernetes
```groovy
process runThisWithDocker {

    container 'busybox:latest'
    containerOptions '--volume /data/db:/db'

    output: file 'output.txt'

    '''
    your_command --data /db > output.txt
    '''
}
```
* To specify CPUs. To retreive the value of a directive, use `task.directive`. Here I use the directive value of `task.cpus` in the command itself
```groovy
process big_job {

  cpus 8
  """
  blastp -query input_sequence -num_threads ${task.cpus}
  """
}
```
* In order to see standard output from commands, use directive `echo true`
* Directive `errorStrategy` can be used to do different things when a command fails
* The `executor` directive is used to define the computing environment (cluster, kubernetes, etc). Reference [here](https://www.nextflow.io/docs/latest/process.html#executor)
* I can set directives dynamically. Here, if `entries > 100`, it will be sent to the "long" queue, if not, it will be sent to the "short" queue
```groovy
process foo {

  executor 'sge'
  queue { entries > 100 ? 'long' : 'short' }

  input:
  set entries, file(x) from data

  script:
  """
  < your job here >
  """
}
```

## Channels
Data flows through a Nextflow pipeline through channels.  
Channel types:
* **queue**: Non-blocking unidirectional channel. Created by `from`, `fromPath`. They are also created by `into` from the output of a process. Given their nature, queues are used only once. If I want to send data from one output to several inputs, use `into`
* **value**: Channel that can be read several times. This can be used as input by several processes. Created by `value`, `first`, `last`, `collect`, `count`, `min`, `max`, `reduce`, `sum`. Value channels are implicitly created when input specifies a simple value in `from`. Also, output channels are `value` if they come from `value` channels. For example, the following will create a `value` channel in output, because its input is a `value` channel
```groovy
process foo {
  input:
  val x from 1
  output:
  file 'x.txt' into result

  """
  echo $x > x.txt
  """
}
```
* Methods for explicitly creating channels [here](https://www.nextflow.io/docs/latest/channel.html#channel-factory)
### Channel methods
* `channel.bind("value")` or `channel << "value"`: bind value to channel
* `channel.subscribe {function $it}`: executes `function` whenever a value is emitted by a channel, where `$it` is the value being emitted
* Subscribe can run functions at different stages of value emission. Reference [here](https://www.nextflow.io/docs/latest/channel.html#onnext-oncomplete-and-onerror)

### Channel operators
Methods that allow make connections between channels, or transform values emitted by a channel, e.g. filtering.
* Filtering Reference [here](https://www.nextflow.io/docs/latest/operator.html#filtering-operators). 
* For operators that transform the items emitted by a channel, see [here](https://www.nextflow.io/docs/latest/operator.html#transforming-operators). Some examples are list flattening, grouping, and mapping functions. There is an interesting one that parses csv lines and send the data as chunks. There are similar operators to split Fasta and Fastq
    * There are operators that combine elements from channels that have the same key are emitted together. For example, the cross operator:
```groovy
source = Channel.from( [1, 'alpha'], [2, 'beta'] )
target = Channel.from( [1, 'x'], [1, 'y'], [1, 'z'], [2,'p'], [2,'q'], [2,'t'] )

source.cross(target).view()
```
Will output:
```
[ [1, alpha], [1, x] ]
[ [1, alpha], [1, y] ]
[ [1, alpha], [1, z] ]
[ [2, beta],  [2, p] ]
[ [2, beta],  [2, q] ]
[ [2, beta],  [2, t] ]
```
* The `collectFile` operator allows me to gather the items from a channel and put them in a file. The file name can be specified dynamically. This will create files named like letters of the alphabet, and store items that start with each letter in the corresponding file
```groovy
Channel
   .from('Hola', 'Ciao', 'Hello', 'Bonjour', 'Halo')
   .collectFile() { item ->
       [ "${item[0]}.txt", item + '\n' ]
   }
   .subscribe {
       println "File ${it.name} contains:"
       println it.text
   }
```
* The `combine` operator does cartesian product of the elements of two channels
* `concat`: concatenate items of two channels into one
#### Forking operators
* `branch`: set conditions and send items to different channels. Below the condition, I can add a return statement, in case I need to modify the item before sending it to the channel
```groovy
Channel
    .from(1,2,3,40,50)
    .branch {
        small: it < 10
        large: it < 50
        other: true
    }
```
* `into` connects a source channel to two or more targets
```groovy
Channel
     .from( 'a', 'b', 'c' )
     .into{ foo; bar }

 foo.view{ "Foo emit: " + it }
 bar.view{ "Bar emit: " + it }
 ```

 ## Executors
 * **Local**: Runs where the nextflow script was run. It will paralelize using threads
 * See the HPC environments [here](https://www.nextflow.io/docs/latest/executor.html#sge)
 * **AWS batch**: Uses docker to run tasks. See [here](https://www.nextflow.io/docs/latest/awscloud.html#awscloud-batch)

 ## Configuration
 I can se config parameters using arguments when invoking nextflow. I can also use a configuration file as `-c configFile.txt`
 * I can scope configurations by dot prefixing or grouping with curly braces
 ```groovy
 alpha.x  = 1
alpha.y  = 'string value..'

beta {
     p = 2
     q = 'another string ..'
}
```
### Scopes
* `env`: define environment variables
* `params`: parameters accesible in the pipeline script
* `process`: default parameters for processes
    * I can set parameters for all processes that have a `label` directive:
```groovy
process {
    withLabel: big_mem {
        cpus = 16
        memory = 64.GB
        queue = 'long'
    }
}
```
* I can set parameters to multiple labels using regex
```groovy
process {
    withLabel: 'foo|bar' {
        cpus = 2
        memory = 4.GB
    }
}
```
* `!foo` applies to labels that are not foo
* config for the `executor` scope can be found [here](https://www.nextflow.io/docs/latest/config.html#scope-executor)
* I can setup a mail server for notifications on the `mail` scope
* Look into [DSL](https://www.nextflow.io/docs/latest/dsl2.html) 2, a language enhancment. Maybe useful for using modules, and defining the logic an execution of pipelines separately
* I can run a REPL of nextflow with `nextflow console`
* Run pipeline with `nextflow run pipeline.nf`
* `nextflow -with-report`: create html report after run
* `nextflow -with-docker image`: run the pipeline inside a specified docker image. More info [here](https://www.nextflow.io/docs/latest/docker.html#)

# Sequera labs tutorial
Here, I will practice the tutorial by following the Nextflow training by Sequera labs in [YouTube](https://www.youtube.com/watch?v=8_i8Tn335X0)
