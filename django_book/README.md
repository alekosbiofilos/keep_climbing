# Django for beginners
[code](https://github.com/wsvincent/djangoforbeginners)
## Setup
* Create project: `django-admin startproject project_name .`
* Create app: `python manage.py startapp app_name`
* Add app to `config/settings.py"
* `urls.py` in each app
```python
from django.urls import path
from .views import homePageView

urlpatterns = [
    path("", homePageView, name="home")
]
```
* and in `config/urls/py`
```python
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("pages.urls"))
]
```
* Add django support in Pycharm settings

## Templates
* Inside the folder of an app, create `templates/`, and then the templates inside it
* To just display an html file
```python
from django.views.generic import TemplateView

class HomePageView(TemplateView):
    template_name = "home.html"
```
* And then in `urls.py`:
```python
path("", HomePageView.as_view(), name="home")
```
* To have a global template directory, add it to `config/settings.py`
```python
TEMPLATES = [{'DIRS': [BASE_DIR / "templates"]}]
```

## Testing
* `tests.py` in each app`s folder
* This tests that the pages exist. If no db is involved, use `SimpleTestCase`. When dealing with db data, use `TestCase`
* Only functions starting with "test" will be considered tests
```python
from django.test import TestCase, SimpleTestCase

# Create your tests here.

class SimpleTests(SimpleTestCase):
    def test_home_page_status_code(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_about_page_status_code(self):
        response = self.client.get("/about/")
        self.assertEqual(response.status_code, 200)
```
* Tests with database (it creates mock items and test that the database is working)
* Note: If the test involves a view or something with a model, I have to use TestCase, even if the tests don't need the database
* NOTE: The `setUp` function is like an `__init__()` for tests. I define my self things there
```python
from django.test import TestCase
from posts.models import Post

class PostModelTest(TestCase):
    def setUp(self):
        Post.objects.create(text="test post")

    def test_text_content(self):
        post=Post.objects.get(id=1)
        expected_object_name = f"{post.text}"
        self.assertEqual(expected_object_name, "test post")
```
* Include tests where the name of the url is checked (use `reverse()`)
```python
class PostPageExists(TestCase):
    def test_proper_location(self):
        resp = self.client.get("/posts/")
        self.assertEqual(resp.status_code, 200)

    def test_url_name(self):
        resp = self.client.get(reverse("posts"))
        self.assertEqual(resp.status_code, 200)
    # check that correct template is used
    def test_correct_template(self):
        resp = self.client.get(reverse("posts"))
        self.assertTemplateUsed(resp, "posts.html")
```
* To run tests on post methods
```python
def test_post_create_view(self):
    resp = self.client.post(reverse("post_new"), {
        "title": "test create title",
        "body": "test create text",
        "author": self.user.id
    })
    self.assertEqual(resp.status_code, 302)
    self.assertEqual(Post.objects.last().title, "test create title")
    self.assertEqual(Post.objects.last().body, "test create text")
```
* Tests for updates (and arguments in reverse)
```python
def test_post_update_view(self):
    resp = self.client.post(reverse("post_edit", args="1"), {
        "title": "new title",
        "body": "new body"
    })
    self.assertEqual(resp.status_code, 302)
```
* Run tests with `python manage.py test`

## Admin
* Start with `python manage.py createsuperuser`
* Register the apps that I want in the admin page by adding them in `app_name/admin.py`
```python
from django.contrib import admin
from .models import Post
# Register your models here.
admin.site.register(Post)
```
## DB
* When running `makemigrations`, treat it as commits. One migration for each functionality change in specific apps. To make migrations for one app: `python manage.py app_name`.
* In each model,I can specify the string by which each record is displayed in admin
```python
class Post(models.Model):
    text = models.TextField()

    def __str__(self):
        return self.text[:50]
```
* To use default users db, use inside model
```python
class Post(models.Model):
    author = models.ForeignKey(
        "auth.User",
        on_delete=CAS
    )
```
## Forms
When using `CreateView` or `UpdateView`, the class returns a `form` object for the template


## Views
### ListView
Returns a list of items in the model as `posts_list` in the template
```python
from django.views.generic import ListView
# Create your views here.
from posts.models import Post


class PostView(ListView):
    model = Post
    template_name = "posts.html"
    context_object_name = "posts_list"
```
### DetailView
Returns an object for an item of the model. It returns an object named lowercase the model name
```python
 class BlogDetailView(DetailView):
     model = Post
     template_name = "post_detail.html"
```
It has to be called like this from `urls.py`
```python
urlpatterns = [
    path("post/<int:pk>", BlogDetailView.as_view(), name="post_detail")   
]
```
And in the template:
```html
<a href="{% url 'post_detail' post.pk %}">{{ post.title }}</a>
```
### CreateView
This work pretty much the same for `UpdateView` and `DeleteView`
```python
class BlogCreateView(CreateView):
    model = Post
    template_name = "post_new.html"
    fields = ["title", "author", "body"]
```
This will return a `form` object in template
```html
    <h1>New Post</h1>
    <form action="" method="post">
    {% csrf_token %}
    {{ form.as_p }}
        <input type="submit" value="Save">
    </form>
```
In order to set the url where the user is going to be sent to after filling the form, set it in the model
```python
# in models.py
class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(
        "auth.User",
        on_delete=CAS
    )
    body = models.TextField()

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse("post_detail", args=[str(self.id)])
```
### DeleteView
A small thing on `DeleteView`. After deletion, tell the app where to go with `success_url`. I also use `reverse_lazy`, so the url is executed after the deletion is completed
```python
class BlogDeleteView(DeleteView):
    model = Post
    template_name = "post_delete.html"
    success_url = reverse_lazy("home")
```
## Static files
*Note*: Sample config under the `beginner` project.  
By default, Django looks for static dir under each app. To allow global static files, include this in `settings.py`
```python
STATICFILES_DIRS = [
    str(BASE_DIR.joinpath("static")), # global static dir
    "static" # specific to each app
]
```
Set paths in `settings.py`
```python
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    str(BASE_DIR.joinpath("static")),
    "static"
]
STATIC_ROOT = str(BASE_DIR.joinpath("staticfiles"))
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"
```
Then run `python manage.py collectstatic`. That command will store all static files at `staticfiles/` under the root directory

### Serve using whitenoise
1. `conda install whitenoise`
2. Add `whitenoise.runserver_nostatic` to `INSTALLED_APPS`
3. Add `whitenoise.middleware.WhiteNoiseMiddleware` to MIDDLEWARE
4. Change `STATICFILES_STORAGE` to `WhiteNoise`

## Users
In `config/urls.py`, add the authentication URLs
```python
path("accounts/", include("django.contrib.auth.urls"))
```
By default, Django will look for `templates/registration/login.html` at the root dir.  
In `config/settings/py`, specify where the user is going to be redirected to after logging in and out
```python
LOGIN_REDIRECT_URL = "home"
LOGOUT_REDIRECT_URL = "home"
```
Some views/urls are automatically generated
```html
{% if user.is_authenticated %}
    <p>Hi {{ user.username }}</p>
    <a href="{% url 'logout' %}">Log out</a>
{% else %}
    <p>You are not logged in</p>
    <a href="{% url 'login' %}">Log in</a>
{% endif %}
```
### Sign up
1. Make an "accounts" app
2. Create view. Here, I specify the form class.
NOTE: URLs are not loaded as generic classes are, so it is better to use `reverse_lazy` for URLs, so they can be accessed once they are available
```python
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

# Create your views here.

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "signup.html"
```

### HOWEVER
It is recommended to use a custom user model (code in custom_user project)
1. Make `accounts` app
2. Add `AUTH_USER_MODEL = "accounts.CustomUser"` to `settings.py`
3. In `accounts/models.py`
```python
from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)
```
NOTE: `null` means to allow null values in the db. `blank` means to allow empty values in forms.  
Forms: User forms are present in two places: in the signup form, and in the admin form. So I have to modify both.  
In models:
```python
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from accounts.models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        # to add fields
        fields = UserCreationForm.Meta.fields + ("age",)
    
    
class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields
```
In `accounts/admin.py`
```python
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from accounts.forms import CustomUserCreationForm, CustomUserChangeForm
from accounts.models import CustomUser
# Register your models here.


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    # Control which fields are shown
    list_display = ["email", "username", "age", "is_staff"]
    fieldset = UserAdmin.fieldsets + ((None, {"fields", ("age",)}),)
    add_fieldsets = UserAdmin.add_fieldsets + ((None, {"fields", ("age",)}),)


admin.site.register(CustomUser, CustomUserAdmin)
```
To use the default login pages, I have to put the templates under `templates/registration/login.html`

## Styling
### Crispy-forms
1. Add `crispy_forms` to `INSTALLED_APPS` in `config/settings.py` 
2. Add `bootstrap4` to `CRISPY_TEMPLATE_PACK`
3. Load and use in templates:
```html
{% extends "base.html" %}
{% load crispy_forms_tags %}
{% block title %}Sign Up{% endblock title %}

{% block content %}
    <h2>Sign Up</h2>
    <form action="" method="post">
    {% csrf_token %}
    {{ form|crispy }}
    <button class="btn btn-success" type="submit">Sign Up</button>
    </form>
{% endblock content %}
```

## Change password
1. Create `password_change_form.html` and `password_change_done.html` under `templates/registration/`
2. In `password_change_form.html`
```html
{% extends "base.html" %}

{% block title %}Password Change{% endblock title %}

{% block content %}
    <h1>Password change</h1>
    <p>Please enter your old password, and confirm the new password</p>
    <form action=""method="post">
        {% csrf_token %}
        {{ form.as_p }}
        <input class="btn btn-success" type="submit" value="Change password">
    </form>
{% endblock content %}
```
3. In `password_change_done.html`
```html
{% extends "base.html" %}

{% block title %}Password Change successful{% endblock title %}

{% block content %}
    <h1>Password change</h1>
    <p>Your password was changed</p>

{% endblock content %}
```
### Password reset
In production, use [SendGrid](https://sendgrid.com). For now, use the console
1. In `settings.py`, add `EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"`
2. Create `password_reset_form.html`, `password_reset_done.html`, `password_reset_confirm.html`, `password_reset_complete.html` under `templates/registration/`

### Send emails to users
1. Open a [Sendgrid](https://sendgrid.com) account
2. In the Sendgrid dashboard, go to "Integrate using our Web API or SMTP"
3. Select SMTP
4. Create API key
5. Look at item 2: it has the info for sending emails using SMTP
6. Create sender identity

#### Add email info to `settings.py`
1. In `config/settings.py`, set 
```python
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
DEFAULT_FROM_EMAIL = "jfoq@pm.me"
EMAIL_HOST = "smtp.sendgrid.net"
EMAIL_HOST_USER = "apikey"
EMAIL_HOST_PASSWORD = "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"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
``` 
2. Customize email text: create `templates/registration/password_reset_email.html`
```html
{% load i18n %}{% autoescape off %}
{% blocktranslate %}You're receiving this email because you requested a password reset for your user account at {{ site_name }}.{% endblocktranslate %}

{% translate "Please go to the following page and choose a new password:" %}
{% block reset_link %}
{{ protocol }}://{{ domain }}{% url 'password_reset_confirm' uidb64=uid token=token %}
{% endblock %}
{% translate 'Your username, in case you’ve forgotten:' %} {{ user.get_username }}

{% translate "Thanks for using our site!" %}

{% blocktranslate %}The "cool website" team{% endblocktranslate %}

{% endautoescape %}
```
3. create `templates/registration/password_reset_subject.txt` and put the subject line of the email

## Articles app
1. `python manage.py startapp articles`
2. Add it to `settings/INSTALLED_APPS`
3. Build model
```python
from django.db import models
from django.db.models import CASCADE as CAS
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.conf import settings

class Article(models.Model):
    title = models.CharField(max_length=250)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        # this captures users db
        get_user_model(),
        on_delete=CAS
    )
    
    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse("article_detail", args=[str(self.id)])
```
4. Add to admin page
```python
from django.contrib import admin
from articles.models import Article
# Register your models here.
admin.site.register(Article)
```

## Permissions and authorization
1. Set author to the logged-in user
   1. In `articles/view.py`.
```python
class ArticleCreateView(CreateView):
    model = Article
    template_name = "article_new.html"
    fields = ("title", "body")
    # Here I can do things when the form is going to be saved
    def form_valid(self, form):
        # I can auto-fill some fields
        form.instance.author = self.request.user
        return super().form_valid(form)
```
2. Limit access to logged-in users. Look for "mixins". They are abstract classes I can use to add functionality to class-based views. In `articles/views.py`. By using the `LoginRequired` mixin, django sends people to the login page if they try to access a part of the site that needs authorization.
```python
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView

from articles.models import Article
class ArticleCreateView(LoginRequiredMixin, CreateView):
    model = Article
    template_name = "article_new.html"
    fields = ("title", "body")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
```
3. Only allow authors of articles to edit or delete them. Use the `UserPassessTestMixin`. With it, I can set a function (`test_func`) that must be passed to allow access to the view. If the test doesnt pass, I get a 403 Forbiden error. In `articles/views.py`
```python
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic.edit import UpdateView
from articles.models import Article

class ArticleUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Article
    fields = ("title", "body")
    template_name = "article_edit.html"
    
    def test_func(self):
        obj = self.get_object()
        return obj.author == self.request.user
```
4. Comments. The idea is to display the comments to an article from different users. To show foreign key relationships inside other tables in admin, use "Inlines". In `articles/admi.py`
```python
from django.contrib import admin

from articles.models import Article, Comment

# Instead of Stacked, there is also TabularInline
class CommentInline(admin.StackedInline):
    model = Comment
    extra = 0


class ArticleAdmin(admin.ModelAdmin):
    inlines = [CommentInline]

admin.site.register(Article, ArticleAdmin)
admin.site.register(Comment)
```
   1. Show comments below each article: Queries. After setting the `related_name`, I can use the object with this name under an article, to get all the comments of that article. For example, in a template `{% for comment in article.comments.all %}`
```python
from django.db import models
from articles.models import Article
from django.contrib.auth import get_user_model
from django.db.models import CASCADE as CAS

class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=CAS, 
                                # related name is the name of the object
                                # that will contain the articles associated
                                # with a comment
                                related_name="comments")
    comment = models.CharField(max_length=140)
    author = models.ForeignKey(get_user_model(), on_delete=CAS)
```
## Deployment

### Checklist
* Configure static files
* Update ALLOWED_HOSTS
* Install `Gunicorn`
* Create Procfile
* Set `DEBUG = False`
* Hide `SECRET_KEY`
* Use production db
More at the [Django deployment checklist](https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/)
#### Use environment variables
1. Install environs: `conda install environs`. and in `config/settings.py`
```python
from environs import Env

env = Env()
env.read_env()
```
2. Create `.env` file in the same directory as `manage.py`. Then, for each variable, use `export VAR=value`
3. Make sure it is not tracked by git
4. Now, with the variables in `.env`, I set them in Python using the syntax:
```python
from environs import Env

env = Env()
env.read_env()
# here, I am assigning the boolean value of the variable DEBUG in .env to the variable some_var
# in case the variable DEBUG does not exist, set it to True
some_var = env.bool("DEBUG", default=True)
```

#### Debug and allowed hosts
1. In `config/settings.py`: `DEBUG = False`
2. Add the domains/IPs from where the app will be served to `ALLOWED_HOSTS` in `config/settings.py`

#### Database
1. Install PostgreSQL adapter: `conda isntall psycopg2-binary`
2. Set it on `config/settings.py`

#### Static files
See static files section above: sample config in the `beginner` project

## 3rd party packages
* [Django website](https://djangopackages.org)
* [Author repo](https://github.com/wsvincent/awesome-django)