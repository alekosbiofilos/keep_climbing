# Django for professionals

## Docker
1. Make virtual environment and install django
2. Start Django project, and add an app
3. Create `Dockerfile` at the base of django project
4. Create environment file with `pip list --format=freeze > environment.txt`
5. Create `Dockerfile`

```dockerfile
# Pull base image
FROM python:3.8

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set working directory
WORKDIR /code

# Install dependencies
COPY environment.txt /code/
RUN pip install -r environment.txt

COPY .. /code/
```
6. Run `sudo docker build .`
7. Create `docker-compose.yml`

```yaml
version: "3.8"

# each service is a container to be built
services:
  web:
    # this is important, because that allows me to edit files
    # the number should correspond to my id `id -u alekos`
    user: "1000"
#    build is the directory where the Dockerfile is
    build: .
#    what to run when in the container
    command: python /code/manage.py runserver 0.0.0.0:8000
    volumes:
      - type: volume
        source: django_project
        target: /code
    ports:
      - "8000:8000"

volumes:
  django_project:
```
8. Run `sudo docker-compose up`
9. I might have to add `"0.0.0.0:8000"` to `ALLOWED_HOSTS` in `config/settings.py`, and run it locally at `127.0.0.1:8000`
10. When done, stop the server and run `sudo docker-compose down`

## PostgreSQL
In folder `postgresql`
1. Create environment: `pipenv install django`
2. Activate environment: `pipenv shell`
3. Create project: `django-admin startproject config .`
4. Create `Dockerfile` and `docker-compose.yml`
```dockerfile
FROM python:3.8

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /code

COPY Pipfile Pipfile.lock /code/

RUN pip install pipenv && pipenv install --system

COPY . /code/
    
```
```yaml
version: "3.8"

services:
  web:
#    build is the directory where the Dockerfile is
    build: .
#    what to run when in the container
    command: python /code/manage.py runserver 0.0.0.0:8000
    volumes:
      - .: /code
    ports:
      - "8000:8000"
    # This is important
    depends_on:
      - db

```
5. `sudo docker build`
6. `docker-compose up`. When updating packages, I have to pass `--build`, so that `docker-compose` does not use the cached version, but build new containers
7. Run commands inside a service in the container: `docker-compose exec web python manage.py createsuperuser`
8. Stop container: `docker-compose down`

### Switch to PostgreSQL
1. Add service to docker-compose (in addition to web)
```yaml
services:
  db:
    image: postgres:11
    environment:
      - "POSTGRES_HOST_AUTH_METHOD=trust"
```
2. Change `config/settings.py`
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        "NAME": "postgres",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "db",
        "PORT": 5432
    }
}
```
3. Install the PostgreSQL driver: `sudo docker-compose exec web pipenv install psycopg2-binary`. It is better to install everything from inside the container. However, this did not work. What I did was to add the package to `Pipfile`, and then do `pipenv lock` locally.

## Book app
### Initial setup
In `books/`
1. `touch Pipefile` 
2. `pipenv install django psycopg2`
3. `pipenv shell`
4. `django-admin startproject config .`
5. `python manage.py runserver`
6. Generate `Dockerfile` and `docker-compose.yml`
7. When running `docker-compose up --build`, first run `newgrp docker` and `sudo gpasswd -aG $USER docker`, so that I can run `docker-compose` without sudo

### Custom User Model
1. Make sure to create custom user model before the first migration
2. `python manage.py startapp accounts`
```python
# in accounts/models.py
from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.

class CustomUser(AbstractUser):
    pass
```
3. Add to `INSTALLED_APPS` and put this at the end of `config/settings.py`
```python
AUTH_USER_MODEL = "accounts.CustomUser"
```
4. Create migrations: `docker-compose exec web python manage.py makemigrations accounts`
5. Migrate: `docker-compose exec web python manage.py migrate`
6. Point the admin app to the custom user model. Make `accounts/forms.py`
```python
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = ("email", "username")


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = get_user_model()
        fields = ("email", "username")
```
7. Set the custom user model in `accounts/admin.py`
```python
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from accounts.forms import CustomUserChangeForm, CustomUserCreationForm

CustomUser = get_user_model()

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ["email", "username"]


admin.site.register(CustomUser, CustomUserAdmin)
```
8. Create admin user: `docker-compose exec web python manage.py createsuperuser`

#### Tests
* In `tests.py` under each app
* Methods should start with `test_`
* In `accounts/tests.py`
```python
from django.contrib.auth import get_user_model
from django.test import TestCase


# Create your tests here.

class CustomUserTests(TestCase):
    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(
            username="user",
            email="user@example.com",
            password="testpass123"
        )
        self.assertEqual(user.name, "user")
        self.assertEqual(user.email, "user@example.com")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):
        User = get_user_model()
        admin = User.objects.create_superuser(
            username="superuser",
            email="super@user.com",
            password="superpass123"
        )

        self.assertEqual(admin.username, "superuser")
        self.assertEqual(admin.email, "super@user.com")
        self.assertTrue(admin.is_active)
        self.assertTrue(admin.is_staff)
        self.assertTrue(admin.us_superuser)
```
* With the container up. Do `docker-compose exec web python manage.py test`

### Pages app
* In `config/settings.py`, add `'DIRS': [str(BASE_DIR.joinpath("templates"))]` to `TEMPLATES` so django look for templates at the base dicrectory/templates
* Start name of html files to be inherited with _
* After updating a `urls.py`. I have to do
  1. `docker-compose down`
  2. `docker-compose up -d`. this runs in detached mode. To see ouptut, tun `docker-compose logs`

#### Tests
Use `SimpleTestCase` for apps that dont have models
* In `pages/tests.py`. To test things on only one app, do `python manage.py test pages`
```python
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from pages.views import HomePageView

class HomepageTests(SimpleTestCase):
  # Use function setUp as __init__
    def setUp(self):
        self.url = reverse("home")
        self.response = self.client.get(self.url)

    def test_homepage_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_homepage_url_name(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_homepage_template(self):
        self.assertTemplateUsed(self.response, "home.html")

    def test_contents_correct_html(self):
        self.assertContains(self.response, "Homepage")

    def test_contents_incorrect_html(self):
        self.assertNotContains(self.response, "this should not be here")
    # This checks that the correct view resolves the correct path
    def test_homepage_url_resolves_view(self):
        view = resolve("/")
        self.assertEqual(
            view.func.__name__, HomePageView.as_view().__name__
        )
```

## User registration
* Add `path(include("accounts", include("django.contrib.auth.urls"))),` to `config/urls.py`. Since I am including URLs from django, there are some URL functionality that alredy works:
```
accounts/login/ [name="login"]
accounts/logout/ [name="logout"]
accounts/password_change/ [name="password_change"]
accounts/password_change/done/ [name="password_change_done"]
accounts/password_reset/ [name="password_reset"]
accounts/password_reset/done [name="password_reset_done"]
accounts/reset/<uidb64>/<token> [name="password_reset"]
accounts/reset/done/ [name="password_reset_done"]
```
* Create templates under `templates/registration/`
* In order to tell where to redirect the user after login, set the name of the url at `config/settings.py`: `LOGIN_REDIRECT_URL = "home"`
* Similar for logging out: `LOGOUT_REDIRECT_URL = "home"`
* Create `accounts/urls.py`
```python
from django.urls import path
from accounts.views import SignupPageView

urlpatterns = [
    path("signup/", SignupPageView.as_view(), name="signup")
] 
```
* Add to `config/urls.py`: `path("accounts/", include("accounts.urls")),`
* In `accounts/views.py`
```python
from django.urls import reverse_lazy
from django.views import generic
from .forms import CustomUserCreationForm

class SignupPageView(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"
```
* Tests (on `accounts/tests.py`)
```python
class SignUpPageTests(TestCase):
    def setUp(self):
        url = reverse("signup")
        self.response = self.client.get(url)

    def test_signup_template(self):
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, "registration/signup.html")
        self.assertContains(self.response, "Sign Up")
        self.assertNotContains(self.response, "bad content")

    def test_signup_form(self):
        form = self.response.context.get("form")
        self.assertIsInstance(form, CustomUserCreationForm)
        self.assertContains(self.response, "csrfmiddlewaretoken")

    def test_signup_view(self):
        view = resolve("/accounts/signup/")
        self.assertEqual(
            view.func.__name__, SignupPageView.as_view().__name__
        )
```

## Static assets
* `STATIC_URL`: URL to put in HTML to reference static files
* `STATICFILES_DIRS`: location of static files during development. Set it up in `config/settings.py`
```python
STATICFILES_DIRS = (str(BASE_DIR.joinpath("static")),)
```
* `STATIC_ROOT`: Location of static files in production. Set it in `config/settings.py`
```python
STATIC_ROOT = str(BASE_DIR.joinpath("staticfiles"))
```
* `STATIC_FINDERS`: Tells Django how to look for static files.
  * `FileSystemFinder` looks in `STATICFILES_DIRS`, and `AppDirectoriesFinder` looks in a `static` folder under each app. If I put them explicitly, I can specify where Django looks first. In `config/settings.py`
```python
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder"
]
```
* At the beginning of each page put `{% load static %}`. Then, reference static files like
```html
<link rel="stylesheet" href="{% static 'css/base.css' %}">
```
* When deploying, run `docker-compose exec web python manage.py collectstatic`
* Install cryspy forms:
  1. Put `django-crispy-forms` in Pipfile.
  2. `pipenv lock`
  3. `docker-compose down`
  4. `docker-compose up --build`
  5. add `crispy_forms` in `config/settings.py` in `INSTALLED_APPS`
  6. Add `CRISPY_TEMPLATE_PACK = "bootstrap4"` to `config/settings.py`
  7. Use it in templates
```html
{% load crispy_forms_tags %}
<h2>Sign Up</h2>
<form method="post">
  {% csrf_token %}
  {{ form|crispy }}
  <button class="btn btn-success" type="submit">Sign Up</button>
</form>
```

## Advanced User Registration
* Install `django-allauth`
* Add `    "django.contrib.sites", "allauth", "allauth.account",` to `INSTALLED_APPS` in `config/settings.py`
* Add `SITE_ID = 1` to `config/settings.py`
* By default, `AUTHENTICATION_BACKENDS` is set to `"django.contrib.auth.backend.ModelBackend"`. Since I am going to also use allauth, I have to put it explicitly.
```python
AUTHENTICATION_BACKENDS = (
  "django.contrib.auth.backends.ModelBackend",
  "allauth.account.auth_backends.AuthenticationBackend"
)
```
* Another default that can be set up explicitly is `EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"`
* `allauth` uses `ACCOUNT_LOGOUT_REDIRECT` to override logout redirects, so it is better to set it up
```python
ACCOUNT_LOGOUT_REDIRECT = "home"
```
* Change `path("accounts/", include("django.contrib.auth.urls"))` with `path("accounts/", include("allauth.urls"))`
* I could also delete `accounts.urls.py` and `accounts/views.py`
* Move templates from `templates/registration/` to `templates/account/`
* Change url names in `_base.html`
```html
<a class="p-2 text-dark" href="{% url 'account_logout' %}">Log Out</a>
<a class="p-2 text-dark" href="{% url 'account_login' %}">Log In</a>
```
* In `config/settings.py` I can set up `ACCOUNTS_SESSION_REMEMBER`:
  * `None`: asks the user with a "Remember me" box
  * `False`: Do not remember last session
  * `True`: Remember last session
### Logout
* `allauth` will send me to an intermediary logout page. I can customize that template under `templates/account/logout.html`
```html
{% extends '_base.html' %}
{% load crispy_forms_tags %}

{% block title %}Log Out {% endblock title %}

{% block content %}
    <h1>Log out</h1>
    <form method="post" action="{% url 'account_logout' %}">
        {% csrf_token %}
        {{ form|crispy }}
        <button class="btn btn-danger" type="submit">Log out</button>
    </form>
{% endblock content %}
```
### Registration
* I can set the signup page so that it doesnt confirm password by setting `ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False`
* After signing up, a confirmation email will be sent
* In the admin section, in the "Sites" section, I can change domain and display names
* Set up email only registration (no user name). In `config/settings.py`
```python
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_ACCOUNT_UNIQUE_EMAIL = True
```
* In the database, the username will be populated with the part of the email before the @. If that is not unique, `allauth` adds an extra character
### Social media authentication
* [Tutorial for authenticating with GitHub](https://learndjango.com/tutorials/django-allauth-tutorial)

## Environment variables
* Useful for storing sensitive information in the OS and not in the codebase, which might get leaked
* Use the `environs` package. Add to `config/settings.py`
```python
from environs imort Env

env = Env()
env.read_env()
```
* Then, in `docker-compose.yml`, add the environment variables. **Note**: If there is a $ in the string, put another $, so it becomes $$
```yaml
    depends_on:
      - db
    environment:
      - DJANGO_SECRET_KEY=django-insecure-v!ekq_^(ira#jn&s6q6l6q$$^icpu$$j$$kzj+u%+)1o46=joq+*p
```
* And then, point the `config/settings.py` variable to the environment variable
* Similarly, add `DEBUG=True` to the environment variables.
* Add the database URL to the environment

## Emails
* Templates at `templates/account/email`
* When editing the templates, the variables `current_site`, and others, come from the "Sites" section on the admin page. The item to be used is specified by the `SITE_ID` variable in `config/settings.py`
* Change the "mail from" field, with `DEFAULT_FROM_EMAIL` in `config/settings.py`
* Similar templates exist for reset and change passwords under. Look on [Github](https://github.com/pennersr/django-allauth) for "account_reset_password" and see where the templates are.
* Change backend on `config/settings.py` to `EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"`
* Change `EMAIL_HOST`, `EMAIL_HOST_USER`, `EMAIL_HOST_PASSWORD`, `EMAIL_PORT`, `EMAIL_USE_TLS` in `config\settings.py` with instructions from email provider.

## Books app
1. `docker-compose exec web python manage.py startapp books`
2. Register in admin like this, to see more fields in the admin page
```python
from django.contrib import admin

# Register your models here.
from books.models import Book

class BookAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "price")

admin.site.register(Book, BookAdmin)
```
A better way of dealing with detail pages, is to include their absolute url in the model, so they can be more easily referenced in pages.
1. Add function that generates url
```python
# books/models.py
from django.db import models
from django.urls import reverse


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("book_detail", args=[str(self.id)])
```
2. Then, reference the url in the template, like this:
```html
{% extends '_base.html' %}
{% block title %}Books{% endblock title %}

{% block content %}
  {% for book in book_list %}
    <div>
      <h2><a href="{{book.get_absolute_url}}">{{ book.title }}</a></h2>
    </div>
  {% endfor %}
{% endblock content %}
```
### IDs vs PK
IDs are part of a model, and auto-increment by default. PK is a field in the model (ID by default) used to define each element. It is better and more secure to use UUID for pk
```python
import uuid
from django.db import models
from django.urls import reverse


class Book(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
```
And then, fix `books/urls.py`
```python
path("<uuid:pk>/", BookDetailView.as_view(), name="book_detail")
```

## Reviews app
### Foreign keys
* One-to-one: Each element of one table, matches only one element of another table
* One-to-many: Default foreign key. One element of a table can have more than one element in another table.
* Many-to-many: An element in one table, can have several elements in another table, and an element on the other table can have many elements in the firs table.
* To make a one-to-many link, follow this code
```python
class Review(models.Model):
    # name the field the same as the linked model: reviews
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="reviews")
    # get_user_model extracts the custom user model
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    review = models.CharField(max_length=255)

    def __str__(self):
        return self.review
```
* In a template, it can be referenced as
```html
    <ul>
      {% for review in book.reviews.all %}
        <li>{{ review.review }} ({{ review.author }}</li>
      {% endfor %}
    </ul>
```

## File uploads
* In django, uploads are called "media"
* For images, install `pillow`
* It is important to validate uploaded files to make sure they are safe
* In `config/settings.py`
```python
MEDIA_URL = "/media/"
MEDIA_ROOT = BASE_DIR / "media"
```
* Make `media/` and `media/covers`
* To be able to see uploads locally, I have to configure the urls. In `config/settings.py`
```python
from django.conf import settings
from django.conf.urls.static import static
# add this to urlpatterns
static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
* Then, in models, I can include this field
```python
cover = models.ImageField(upload_to="covers/")
```
* Now, in a template, I can get the image with:
```html
<img width="300px" class="bookcover" src="{{ book.cover.url }}" alt="{{ book.title }}">
```
* In production, look into how to upload the files to a different CDN

## Permissions
How to make some pages available to logged-in users
* In `books/views.py`
```python
from django.contrib.auth.mixins import LoginRequiredMixin

class BookListView(LoginRequiredMixin, ListView):
    model = Book
    context_object_name = "book_list"
    template_name = "book_list.html"
    login_url = "account_login"
```
* Now, if I go to the books url without being logged-in, I will be redirected to the login page, and after logging in, I will go to the books page
* Custom permissions: Add permissions to the models, so I can assign users to them
* In `books/models.py`, include this inside the book class
```python
class Meta:
    permissions = [
        ("special_status", "Can read all books")
    ]
```
* Now, in admin, I can add users to the `special_status` permission
* Now, I have to set views to be available to users with that permission. In `books/views.py`
```python
from django.views.generic import ListView, DetailView
from .models import Book
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

# the PermissionRequiredMixin has to be after LoginRequiredMixin
class BookDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Book
    context_object_name = "book"
    template_name = "book_detail.html"
    login_url = "account_login"
    # This is the permission specification
    permission_required = "books.special_status"
```
* Because I set that only users with certain permissions could see the book list, some of the tests will fail. to fail change:
```python
from django.contrib.auth.models import Permission

class BookTests(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(
            username="reviewuser",
            email="meh@example.com",
            password="testpass1234!"
        )
        self.permission = Permission.objects.get(codename="special_status")
        self.book = Book.objects.create(
            title="Harry Potter",
            author="JK Rowling",
            price="25.00"
        )
    def test_book_detail_view_with_permissions(self):
        self.client.login(email="meh@example.com", password="testpass1234!")
        # Give permission to user
        self.user.user_permissions.add(self.permission)
        response = self.client.get(self.book.get_absolute_url())
        no_response = self.client.get("books/23")
        # tests...
```

## Search functionality
* In a `ListView` class, there is a `queryset` attribute that by default outputs all the items. Also look at the QuerySet API from the django website. From `books/views.py`.  
In this code, Q is what I use to build queries, and | is "or"
```python
from django.db.models import Q

class SearchResultsView(ListView):
    model = Book
    context_object_name = "book_list"
    template_name = "search_results.html"

    def get_queryset(self):
        return Book.objects.filter(
            Q(title__contains="Beginners") | Q(title__contains="API")
        )
```
* GET vs POST: In a nutshell, I should use GET when the data sent does not affect the state of the database, and POST when the data does.
### Using search forms
Components:
* action: page to redirect when sending the query.
* method: how to send the data.
* name: variable name used to store the data (visible in `views.py`).
```html
<form action="{% url 'search_results' %}" method="get" class="form-inline mt-2 mt-md-0">
  <input name="q" class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="search">
</form>
```
* In `books/views.py`, I can get the variable *q* like this:
```python
class SearchResultsView(ListView):
    model = Book
    context_object_name = "book_list"
    template_name = "search_results.html"

    def get_queryset(self):
        # here
        query = self.request.GET.get("q")
        return Book.objects.filter(
            Q(title__contains=query) | Q(title__contains=query)
        )
```

## Performance
Basic areas where performance can be optimized
* Database queries
* Caching
* Indexing
* compressing front-end assets

### Optimize queries
1. Install `django-debug-toolbar`
2. Add `"debug_toolbar"` to `INSTALLED_APPS` in `config/settings.py`
3. Add `"debug_toolbar.middleware.DebugTollbarMiddleware"` to `MIDDLEWARE`
4. Set `INTERNAL_IPS` to the same IP than the container. In `config/settings.py`
```python
import socket
hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
INTERNAL_IPS = [ip[:-1] + "1" for ip in ips]
```
5. Add debug urls if in development mode. In `config/urls.py`
```python
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

```
Now, I will see a debugging bar at the right.  
When optimizing SQL queries, it is better to make a few big queries, than many small queries.
* `select_related` is used for single-value relationships through a forward one-to-many or one-to-one relationship. Basically it pulls related elements from the element I am querying
* `prefetch_related` is used for a set of objects, like many-to-many or many-to-one. The join is done in Python, before translating to SQL

### Caching
Caching: in-memory storage of expensive calculations. Options: memcached (native Django support), or Redis (available via django-redis). I can cache at the level of the entire site, at the level of views, template fragment caching, and more granular options. Look into it at the Django website

### Indexing
To speed up database performance. It can be done by including `db_index` in the model class. As a rule of thumb, if a given field is used in 10-25% of all queries, it might be a good idea to index that. However, indices take space:
```python
class Book(models.Model):
    ...
    class Meta:
        indexes = [
            models.Index(fields=["id"], name="id_index")
        ]
```
After chaning the file, make and apply migrations.  

## security
In production, I have to set up different environment variables, turn off debug, etc. In order to do that, I use another docker compose file, and do not commit it to git.  
### DEBUG and other environment variables
It is better to leave a production defaults in `config/settings.py`. In that way, if the environment variables dont load, it will default to production settings. For example, `DEBUG` can be set as `DEBUG = env.bool("DJANGO_DEBUG", default=False)`.  
### SECRET_KEY
It is better to generate a secret key every time the app is deployed, to avoid leaving it in docker-compose.yml with:
```python
import secrets
print(secrets.token_urlsafe(38).replace("$", "$$"))
```
This will generate 50 characters secret key. The replace part is important because Django does not like single "$".

### Web security
Look into the Open Web Application Security Project (OWASP) for cheat-sheets and recommendations.
* Set `SECURE_SSL_REDIRECT=True` in production
Make the browser only deal with HTTPS
```python
SECURE_SSL_REDIRECT = env.bool("DJANGO_SECURE_SSL_REDIRECT", default=True)
SECURE_HSTS_SECONDS = env.int("DJANGO_SECURE_HSTS_SECONDS", default=259200)
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
    "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", default=True)

SECURE_HSTS_PRELOAD = env.bool("DJANGO_SECURE_HSTS_PRELOAD", default=True)
```
And in `docker-compose.yml` (the dev docker-compose), set them to `False`, `0`, and `False`
* Secure cookies
```python
SESSION_COOKIE_SECURE = env.bool("DJANGO_SESSION_COOKIE_SECURE", default=True)
CSRF_COOKIE_SECURE = env.bool("DJANGO_CSRF_COOKIE_SECURE", default=True)
```
And set them to `False` in dev `docker-compose.yml`

## Deployment
### Static files
In `config/settings.py`
* Add `"whitenoise.runserver.no_static"` before `'django.contrib.staticfiles'` to `INSTALLED_APPS`
* Add `"whitenoise.middleware.WhiteNoiseMiddleware"` after `'django.middleware.security.SecurityMiddleware'` to `MIDDLEWARE`
* Set `STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
`
* For user-uploaded files, look into django-stores and a CDN provider
### Gunicorn
1. Install Gunicorn
2. Replace command in docker-compose files to: `gunicorn config.wsgi -b 0.0.0.0:8000`.