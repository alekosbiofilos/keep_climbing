from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read access to all logged-in users
        if request.method in permissions.SAFE_METHODS:
            return True
        
        # Write permissions only to the author of a post
        return obj.author == request.user