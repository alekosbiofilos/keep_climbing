from django.test import TestCase
from django.contrib.auth.models import User
from .models import Post


class BlogTests(TestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        testuser1 = User.objects.create_user(
            username="testuser1", password="password")
        testuser1.save()

        test_post = Post.objects.create(
            author=testuser1,
            title="Blog title",
            body="Sample body content"
        )

    def test_blog_content(self) -> None:
        post = Post.objects.get(id=1)
        author = f"{post.author}"
        title = f"{post.title}"
        body = f"{post.body}"
        self.assertEqual(author, "testuser1")
        self.assertEqual(title, "Blog title")
        self.assertEqual(body, "Sample body content")
