# Blog app

## Allow changes to the DB
This follows the notes from the Todo app.  
* When adding the API, it is a good idea to set versions, so that when I make big changes to the API, the users can still use past versions: `path("api/v1/", include("posts.urls"))`
* In views, if I need to allow the user to write, use `ListCreateAPIView` for lists, and `RetrieveUpdateDestroyAPIView`.
* When  going to the endpoints, I can also access a POST section

## Permisions
* Setup login in API environment.
* In `config/urls.py`, add `path("api-auth", include("rest_framework"))`. Now, the API view allows me to log-in/log-out (top right corner).
* I can set permissions at project, view, or model levels.
* To set permissions at the view level:
```python
from rest_framework import generics, permissions

from .models import Post
from .serializers import PostSerializer

class PostList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer
```
* However, it is better to set strict policies at the project level, and loosen them at the view level. To set up permissions that way, edit `config/settings.py`:
```python
REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated"
    ]
}
```

### Custom permissions
In order to allow only the author of a post to edit/delete it, I have to set a custom permission. Django relies on the class `BasePermission`. I can make a permission class per app, so all permissions inherit from it. Here, the `has_object_permission` is "write" permissions. Look into the other methods of the `BasePermission` class
1. Edit `posts/permissions.py`
```python
from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read access to all logged-in users
        if request.method in permissions.SAFE_METHODS:
            return True
        
        # Write permissions only to the author of a post
        return obj.authorn == request.user
```
2. Use the permission in `posts/views.py`
```python
from rest_framework import generics
from .permissions import IsAuthorOrReadOnly

from .models import Post
from .serializers import PostSerializer


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer
```

## User authentication
Authenticating using an API involves passing a unique identifier with each HTTP request.
* Basic authentication: The client passes unencrypted base64-encoded username:password. Also, the server has to check the credentials at every request. Since this in unencrypted, it should only be used under HTTPS
* Session authentication: After logging-in, the server gives the client a session id, that is passed by the client in the HTTP headers. This is still inneficient, and only works for the browser where the user is logged-in.
* Token authentication: After login, the server sends a token as a cookie, which the user stores. To avoid XSS attacks, it should be stored in a cookie with the httpOnly and secure flags.
* JSON Web Tokens (JWT) are better. They can be implemented using third-party apps for Django.
### Setup token authentication
1. In `config/settings.py`, add:
```python

REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated"
    ],
    "DEFAULT_AUTHENTICATION_CLASSES": [
        # The `SessionAuthentication` is needed for the browsable API.
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication"
    ]
}
```
2. Add `"rest_framework.authtoken"` to `INSTALLED_APPS` in `config/settings.py`
3. Stop server, and run `python manage.py migrate`

### Authentication endpoints
1. Install `dj-rest-auth`
2. Add `"dj_rest_auth"` to `INSTALLED_APPS` in `config/settings.py`
3. Include login urls in `config/urls.py`. This will create, for example `api/v1/dj-rest-auth/login`, and other authentication endpoints

### User registration
1. Install `django-allauth`.
2. Add apps to `INSTALLED_APPS`: Order is important
```python
INSTALLED_APPS = [
    # ...
    'django.contrib.staticfiles',
    "django.contrib.sites",
    "rest_framework",
    "rest_framework.authtoken",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "dj_rest_auth",
    "dj_rest_auth.registration",

    # Local apps
    "posts"
]
```
3. Add `EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"` and `SITE_ID = 1` to `config/settings.py`. Django can host several sites in a single project, so SITE_ID is nedded to specify the site where we are using the authentication.
4. Add urls in `config/urls.py`: `path("api/v1/dj-rest-auth/registration/", include("dj_rest_auth.registration.urls"))`. This will make `http://127.0.0.1:8000/api/v1/dj-rest-auth/registration/` available.
5. After registering a new user, a token will be generated.
6. When logging in, a token is generated. This has to be captured by the front-end. Make sure to read on best security practices.

## Viewsets and Routers
* viewset: define several related views once
* routers: generate URLs

### User endpoints
For each endpoint, I have to create:
1. Serializer class for the model
2. Views for the endpoint
3. URL routes

In `posts/serializers.py`
```python

class UserSerializer(serializers.ModelField):
    class Meta:
        model = get_user_model()
        fields = ("id", "username")
```
In `posts/views.py`
```python
class UserList(generics.ListCreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
```
In `posts/urls.py`
```python
urlpatterns = [
    path("users/", UserList.as_view()),
    path("users/<int:pk>/", UserDetail.as_view()),
    path("<int:pk>/", PostDetail.as_view()),
    path("", PostList.as_view())
]
```

### Viewsets
Used to combine logic for multiple related views in a single class. I can use the same class for both list and detail.  
`posts/views.py` can be re-written as:
```python
from django.contrib.auth import get_user_model
from rest_framework import viewsets
from .permissions import IsAuthorOrReadOnly

from .models import Post
from .serializers import PostSerializer, UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class UserView(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
```

Routers work with viewsets to generate URL patterns.  
`posts/urls.py` can be rewritten as:
```python
from django.urls import path

from .views import PostViewSet, UserViewSet
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
# the first argument is the url endpoint
router.register("users", UserViewSet, basename="users")
router.register("", PostViewSet, basename="posts")

urlpatterns = router.urls
```

## Schemas and documentation
* Schema: machine-readable document that outlines available API endpoints and the HTTP verbs they support
* Documentation: Sections added to a schema to make it human-readable

1. Install dependencies: `pipenv install pyyaml uritemplate`
2. Generate static schema: `python manage.py generateschema > openapi-schema.yml`
3. Generate dynamic schema. In `config/urls.py`
```python
# Add
from rest_framework.schemas import get_schema_view
# add to urlpatterns
path("openapi", get_schema_view(
    title="Blog API",
    description="A sample API",
    version="1.0",
), name="openapi-schema")
```
Now, go to [](localhost:8000/openapi) to see the schema of the API

### Documentation
1. Install dependencies: `pipenv install drf-yasg`
2. Add `"drf_yasg"` to `INSTALLED_APPS` in `config/settings.py`
3. In `config/urls.py`

```python
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Blog API",
        description="A sample API",
        version="1.0",
        default_version="v1",
        terms_of_service="https://www.google.com/policies/terms",
        contact=openapi.Contact(email="jfoq@pm.me"),
        license=openapi.License(name="BSD License")
    ), public=True, permission_classes=(permissions.AllowAny,)
    )

urlpatterns = [
    # add
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc")
]
```
Now, go to [](http://localhost:8000/swagger) or [](http://localhost:8000/redoc)