# Python for APIs

## The web
HTTP verbs are things that a website (or server) can do with its data
* POST: create a record
* GET: Read a record
* PUT: Update a record
* DELETE: Delete a record

An API endpoint is an URL with a verb that exposes data.  
An HTTP message consists of a status line, headers, and optional body data
```
GET / HTTP/1.1
Host: google.com
Accept_Language: en-US
```
The first line says "Get data from ` / ` using HTTP version 1.1".  
The two other lines are the headers.  
This is an example of a response, assuming that the homepage is just "Hello world"
```
HTTP/1.1 200 OK
Date: Mon, 16 Nov 2021 12:12:11 GMT
Server: gws
Accept-Ranges: bytes
Content-length: 13
Content-Type: text/html; charset=UTF-8

Hello world
```
The top line is the response line, then there are header lines, and lastly, after a line break, the message.  
Status codes:
* 2xx: Success
* 3xx: the URL has moved
* 4xx: Client error. Bad request by the client
* 5xx: Server error. The server failed to resolve the request

HTTP is a stateless protocol. This means that each request is independent of any previous requests

### REST
REST (representational state transfer) is:
* Stateless
* Support common HTTP verbs
* Returns data in jSON or XML

## Django REST
1. Install `pipenv install djangorestframework`
2. Add `rest_framework` to `INSTALLED_APPS` in `config/settings.py`
3. Create API endpoint: `python manage.py startapp api` (and setup urls...)
4. Setup `views.py`
```python
from django.db.models import query
from rest_framework import generics
from books.models import Book
from .serializers import BookSerializer


class BookAPIView(generics.ListAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
```
5. Setup serializers (`api/serializers.py`). A serializer translate data into a format that is easier to consume over the internet
```python
from rest_framework import serializers
from books.models import Book


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ("title", "subtitle", "author", "isbn")
```
Now, going to `localhost:8000/api` displays the JSON with some other tools