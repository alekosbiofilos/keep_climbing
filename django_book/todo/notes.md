## Previous config
For basic things, look at the books project
## REST config
In `config/settings.py` add
```python
REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.AllowAny"
    ]
}
```
* `todos/urls.py`
```python
urlpatterns = [
    path("<int:pk>/", DetailTodo.as_view()),
    path("", ListTodo.as_view())
]
```
* `todos/views.py`
```python
from rest_framework import generics
from .models import Todo
from .serializers import TodoSerializer


class ListTodo(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer


class DetailTodo(generics.RetrieveAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
```
When going to the endpoint, The `Allow` header tells me that only "read" verbs are allowed.  
* I have to setup CORS, to allow the API to communicate with the front-end, which could be in a different server
1. Install `django-cors-headers`
2. Add `"corsheaders"` to `INSTALLED_APPS`
3. Add `"corsheaders.middleware.CorsMiddleware"` above `"django.middleware.common.CommonMiddleware"`
4. Create a CORS headers whitelist, to allow communication with the React front-end
```python
CORS_ORIGIN_WHITELIST = [
    "http://localhost:3000",
    "http://localhost:8000"
]
```

## Front-end with React
1. Create app with `npx create-react-app frontend`
2. Go to `frontend` and start app with `npm start`
3. Use Axios to handle the GET requests. Install with `npm install axios`
4. Update `frontend/src/App.js`
```js
import React, { Component } from 'react';
import axios from 'axios';


class App extends Component {
  state = {
    todos: []
  };
  // HTTP requests have to be made with componentDidMount
  componentDidMount() {
    this.getTodos();
  }
  getTodos() {
    axios.get("http://127.0.0.1:8000/api/")
      .then(res => {
        this.setState({ todos: res.data })
      })
      .catch(err => {
        console.log(err)
      });
  }
  render() {
    return (
      <div>
        {this.state.todos.map(item => (
          <div key={item.id}>
            <h1>{item.title}</h1>
            <span>{item.body}</span>
          </div>
        ))}
      </div>
    )
  }
}

export default App
```