from django.urls import path
from pages.views import HomePageView, AboutUs

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("about/", AboutUs.as_view(), name="about")
]