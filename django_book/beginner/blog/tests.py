from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from blog.models import Post
# Create your tests here.

class BlogTests(TestCase):
    def setUp(self):
        """
        here I make a mock post, to run the other tests
        :return:
        """
        self.user = get_user_model().objects.create_user(
            username="testuser",
            email="somethin@meh.com",
            password="secret"
        )
        self.post = Post.objects.create(
            title="Test title",
            body="test text",
            author=self.user
        )

    def test_string_rep(self):
        post = Post(title="Test title")
        self.assertEqual(str(post), post.title)

    def test_post_content(self):
        self.assertEqual(f"{self.post.title}", "Test title")
        self.assertEqual(f"{self.post.author}", "testuser")
        self.assertEqual(f"{self.post.body}", "test text")

    def test_post_list_view(self):
        response = self.client.get(reverse("blog"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "test text")
        self.assertTemplateUsed(response, "blog.html")

    def test_post_detail_view(self):
        resp = self.client.get("/blog/post/1")
        no_response = self.client.get("/blog/post/2373")
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(resp, "Test title")
        self.assertTemplateUsed(resp, "post_detail.html")

    def test_post_create_view(self):
        resp = self.client.post(reverse("post_new"), {
            "title": "test create title",
            "body": "test create text",
            "author": self.user.id
        })
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(Post.objects.last().title, "test create title")
        self.assertEqual(Post.objects.last().body, "test create text")

    def test_post_update_view(self):
        resp = self.client.post(reverse("post_edit", args="1"), {
            "title": "new title",
            "body": "new body"
        })
        self.assertEqual(resp.status_code, 302)

    def test_delete_view(self):
        resp = self.client.post(reverse("post_delete", args="1"))
        self.assertEqual(resp.status_code, 302)