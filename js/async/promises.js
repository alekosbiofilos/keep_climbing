const posts = [
    {
        title: "post one",
        body: "this is post 1"
},
{
    title: "post two",
    body: "this is post 2"
}
]

function getPosts() {
    setTimeout(() => {
        let output = "";
        posts.forEach((post, index) => {
            output += `<li>${post.title}</li>`
        });
        document.body.innerHTML = output;
    },1000)
}

function createPost(post) {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            posts.push(post)
            const error = false;
            if(!error) {
                resolve();
            } else {
                reject("Error!")
            }
        },2000)
    });

}


// createPost({
//     title: "third post",
//     body: "this is post 3"
// })
//     .then(getPosts)
//     .catch(err => console.log(err))

async function init() {
    await createPost({
        title: "third post",
        body: "this is post 3"
    })
    getPosts()
}

init()