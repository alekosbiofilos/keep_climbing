# Javascript
Here, I will follow the [Learn Javascript for free](https://scrimba.com/learn/learnjavascript) course on Scrimba

## Basics

* Select elements of DOM
```js
let counter_element = document.getElementById("counter")
// Set text of tag
counter_element.innerText = "some text"
```
* It is better to use `<script src="functions.js"></script>' than to write the js in the html
* Functions can change variables defined outside
* onClick event listener: runs a function when an element is clicked
```html
<html>
    <body>
        <h2 id="count-el">0</h2>
        <button id="increment-btn" onclick="increment()">INCREMENT</button>
        <script src="index.js"></script>
    </body>
</html>
```
```js
// index.js

// get reference of the DOM element
let countEl = document.getElementById("count-el")
// Initialize counter
let count = 0

function increment() {
    count = count + 1
    countEl.innerText = count
    // countEL.textContent is similar, but it is faster, and it retains flanking spaces and hidden values
}
```
* Look at the [MDN](https://developer.mozilla.org)
* When a string is involved in a statement with numbers, numbers are coherced to strings (e.g. 1 + "1" is "11")
* A more general way of selecting elements from DOM: `document.querySelector("#by-id")`. Here, I have to put the hashtag

## Arrays
* Add/remove things to array
```js
// add things to array
let lst = [1,2,3]
let new_array_length = lst.push(4)
// remove the last element
let removed_element = lst.pop()
```
* For loops
```js
let lst = [1,2,3]
    for (i=0; i < lst.length; i++) {
    console.log(lst[i])
}
```
## Functions
* Functions declared with `function fc(){}` can be declared anywhere in the code, not necessarily before calling them
* I can also declare annonymous functions as `()=>{function_code}`

## Booleans
* and is `&&`
* or is `||`

## Objects
* Syntax
```js
let obj = {
    var1: "value",
    var2: 2,
    // methods
    some_method: function() {
        console.log("hola")
    }
}
console.log(obj.var1) // "value"
console.log(obj["var1"]) // same
obj.some_method()
```

## Making a Chrome extension
* Event listeners are more commonly added inside the js (not using onclick). Using the `addEventListener` method. The first argument is the kind of event, and the second is the function to be executed when the event i triggered
```html
<button id="input-btn">SAVE INPUT</button>
```
```js
// const make variables that can not be reassigned
const inputBtn = document.getElementById("input-btn")
inputBtn.addEventListener("click", function(){
    console.log("clicked")
})
```
* Create DOM elements
```js
let lead = "some value"
// I can also do ulEl.innerHTML = <li>something</li>
let list_item = document.createElement("li")
list_item.textContent = lead
ulEl.appendChild(list_item)
```
* Limiting the times I manipulate the DOM improves performance
* When working with template strings `` `string and ${variable}` ``), I can break the template string in multiple lines
* To deploy the extension, make `manifest.json`
    * `action_popup` points at what to do when I click the extension button
    * In the css, setup the min-width of body, so the extension knows how big the popup should be
    * Go to [chrome://extensions](chrome://extensions), and turn on developer mode
    * To load the extension, go to "Load unpacked", and choose the folder where the extension is

## LocalStorage
* For storing data across page-refresh
* `localStorage` is an object available in global scope
* `localStorage.getItem("name_of_item")`: returns the item
* `localStorage.setItem("item", "data")`: sets value of item: it has to be a string
* `localStorage.clear()`: clears the local storage
* To store arrays in localStorage, conver the array to json with `JSON.stringify`
* To consume json from localStorage: `JSON.parse(localStorage.getItem("item"))`
* To check if a localStorage item exist, test if `localStorage.getItem("item")`: if it doesnt exist, it will return `null`
    * Note: `null` is empty, and `undefined` is not defined (e.g. `let var` with no assignment)


## Chrome API
* I can grab information about the current session in Chrome using the [Chrome API](https.developer.chrome.com)
1. Change the `manifest.json` to ask Chrome permission to get the tab url
```json
{
    "permissions": ["tabs"]
}
```
2. Get chrome tab
```js
chrome.tabs.query({active:true, currentwindow: true}, function(tabs) {
    // and then process tabs here
})
```
## Async things
The course above was good to get me started in JS, but I did not see anything about async/promises. In the following sections I will be following Traversy Media`s [video](https://www.youtube.com/watch?v=PoRJizFvM7s) on the subject.  
The idea is that the code can keep going while it is waiting for another process to happen

### With callbacks
In this context, a callback is a function that is called right after another. In the example (`async/callbacks.js`), `getPosts` is run after I create a post, so the html is rendered again.  
In the example, timeouts are simulating network traffic. So with callbacks, the process of fetching each post starts only after the create post process has ended
```js
const posts = [
    {
        title: "post one",
        body: "this is post 1"
},
{
    title: "post two",
    body: "this is post 2"
}
]

function getPosts() {
    setTimeout(() => {
        let output = "";
        posts.forEach((post, index) => {
            output += `<li>${post.title}</li>`
        });
        document.body.innerHTML = output;
    },1000)
}

function createPost(post, callback) {
    setTimeout(()=>{
        posts.push(post)
        callback()
    },2000)
}

createPost({
    title: "post 3", body: "this is post 3"
}, getPosts)
```

### Promises
With promises, you return a promise that will do something if it resolves, and another thing if it doesnt
```js

function createPost(post) {
    // create promise
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            posts.push(post)
            // error checking
            const error = false;
            // what to do if the promise gets a successful value
            if(!error) {
                resolve();
            } else {
                // what to do if it doesnt
                reject("Error!")
            }
        },2000)
    });

}

createPost({
    title: "third post",
    body: "this is post 3"
})
    .then(getPosts)
    .catch(err => console.log(err))
```
The promise has a `then()` method, where I can set what happens when the promise is fulfilled, and I can chain a `catch()` method to specify what happens if the promise fails

* **Multiple promises**: If I want to process several promises at once, I can set the `then().catch()` to all of them using `Promise.all([array, of, promises])`
```js
const promise1 = Promise.resolve("Hello world");
const promise2 = 10
const promise3 = new Promise((resolve, reject) => setTimeout(resolve, 2000, "Goodbye"))

Promise.all([promise1, promise2, promise3]).then((values) => console.log(values))
```
The code above will return a list of three items, with the values of the promises. However, it will be returned in 2 seconds, because that is the time the longest promise will take

### Await/async
Basically, it runs `createPost()` and when it finishes, it runs `getPosts()`. This is another way of running async functions without the `then().catch()` thing
```js
async function init() {
    await createPost({
        title: "third post",
        body: "this is post 3"
    })
    getPosts()
}

init()
```