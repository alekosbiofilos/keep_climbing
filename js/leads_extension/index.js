const inputBtn = document.getElementById("input-btn");
const tabBtn = document.getElementById("tab-btn");
const deleteBtn = document.getElementById("delete-btn");
const inputEl = document.getElementById("input-el");
let ulEl = document.getElementById("list");

let myLeads = [];
if (localStorage.getItem("myLeads") != null) {
    myLeads = JSON.parse(localStorage.getItem("myLeads"))
    renderLeads(0)
}


inputBtn.addEventListener("click", function(){
    let lead = inputEl.value
    if (lead.length){
        myLeads.push(lead)
        inputEl.value = ""
        start_index = myLeads.length - 1;
        renderLeads(start_index);

    }
})


deleteBtn.addEventListener("click", ()=> {
    console.log("delete")
    myLeads = []
    localStorage.clear()
    list_items = ulEl.children
    while (list_items.length){
        ulEl.removeChild(ulEl.firstChild)
    }
    renderLeads(0)
})

tabBtn.addEventListener("click", ()=>{
    chrome.tabs.query({active:true, currentWindow: true}, function(tabs) {
    myLeads.push(tabs[0].url)
    renderLeads(myLeads.length - 1)
    })
})

function renderLeads(start) {
    for (i=start; i<myLeads.length; i++){
        let lead = myLeads[i]
        let list_item = document.createElement("li")
        let link = document.createElement("a")
        link.href = `https://${lead}`
        link.target = "_blank"
        link.textContent = lead
        list_item.appendChild(link)
        ulEl.appendChild(list_item)
        if (myLeads.length > 0) {
            localStorage.setItem("myLeads", JSON.stringify(myLeads))
    }
    }
}
