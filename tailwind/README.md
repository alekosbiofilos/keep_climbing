# Tailwind from Fireship
I will follow Fireship's [video on TailwindCSS](https://www.youtube.com/watch?v=pfaSUYaSgRo)

## Setup
Tailwind behaves very well with components frameworks. For this tutorial, I will use React. The fireship tutorial says to install craco, but it did not work, so I followed the React setup instructions from Tailwind's [site](https://tailwindcss.com/docs/guides/create-react-app)
1. Create project: `npx create-react-app headwind`
2. Install tailwind: `npm install -D tailwindcss postcss autoprefixer`
3. Create Tailwind config: `npx tailwindcss init -p`
4. Config `tailwind.config.js`
```js
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  theme: {
    extend: {},
  },
  plugins: [],
}
```
5. Replace contents of `src/index.css` with
```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```
6. Check installation: `npm run start`

* `tailwind.config.js` can be used to customize colors, plugins, etc.
    * `mode: "jit"`, it will compile the CSS on-the-fly, reducing compiling time
    * `purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html]`: Remove unused styles, reducing CSS file size. Where to find markup that consume the CSS. PurgeCSS will look into those files, and remove all the classes that are not consumed in them.
      * Note: PurgeCSS tries to find literal strings, so class names build dynamically will not be included. Look for PurgeCSS config. There is an option to never remove a list of classes
    * I think these two things are not necessary anymore

## Classes
* Tailwind has a lot of classes with set values. If I want to put a value that is not there, I can use the value in square brackets: `<div className="fixed top-[23px]">`
* Classes that start by
    * `p`: padding.
    * `m`: margin (`mx` for top + bottom, `my` for left + right)
    * `bg`: background
    * `text`: text
    * `sm`: for small screens (there are also `md`, `lg`, `xl`, `2xl`)
 ## Customize color palette
 1. In `tailwind.config.js`
 ```js
 module.exports = {
  mode: "jit",
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  theme: {
    extend: {
      colors: {
        primary: "#202225",
        secondary: "#5865f2"
      }
    },
  },
  plugins: [],
}
```
2. Now, Tailwind will give me utility classes with those colors (e.g. `bg-primary`)
* However, I can import more colors from Tailwind with `const colors = require("tailwind/colors")`, and then in the colors section:
```js
{
    gray: colors.trueGray
}
```
* Also, to make nested color (`grey-500`), I use:
```js
colors: {
    gray: {
        DEFAULT: "#202225",
        500: "#202225"
    }
}
```
* The `DEFAULT` key will assign a value to `grey` without any dash things
* To create font families in Tailwind, I uadd to `theme.extend`:
```js
fontFamily : {
  headline: "fontname sans-serif"
}
```
* This will create classes like `font-headline`
## Animated icons
1. In order to avoid using loose Tailwind classes, I can group them and define my own classes. For example, here I create the class `sidebar-icon` by combining several Tailwind utilities. In `index.css`
```css
@layer components {
  .sidebar-icon {
    @apply relative flax items-center justify-center
  }
}
```
* I can use CSS pseudo-classes with `hover:bg-green-600`
* In order to animate, I need properties that change between no-hover and hover, and then set the `animate-all` (or other `animate-`). I can also use Tailwind utilities to customize the properties of the animation
```css
.some-element {
    rounded-3xl hover:rounded-xl transition-all duration-300 ease-linear;
}
```
* With tailwind groups, I can apply classes to a child, when the state of its parent changes. Careful that this does not work under `apply`. Here, The scale of the `sidebar-tooltip` will go to 100 when its parent is hovered. Baiscally I set the class `group` to the parent, and then in this case `group-hover:property-to-change` to its child
```html
  <div className="sidebar-icon group">
    {icon}
    <span class="sidebar-tooltip group-hover:scale-100">
      {text}
    </span>
  </div>
```

## From the Tailwind labs YT
Here are some extra notes from the Tailwind labs [YouTube channel](https://www.youtube.com/channel/UCOe-8z68tgw9ioqVvYM4ddQ)
* Therea are spacing utilities, like `space-y-5`, which I can put in a parent, so all the children will be vertically-spaced

### Responsive design
* The screen size classes are `min-width` media queries. So applying `sm` will apply to the small breakpoint and higher
* The classes should be set with `screenBreakpoing:className` (e.g. `sm:bg-gray`). Breakpoints: `sm`, `md`, `lg`, `xl`, `2xl`

#### Change layout
* To hide an element on a breakpoint: `lg-hidden`
* Split layout in half: Use css grid with breakpoints
* If I want to chnge the order of items in two layouts, I can put the item to be changed in both places, and do `lg:hidden` to hide the item in large breakpoint. To show an element, I do `lg:block` for example.
* Another application of `hidden`. If I want a newline at a breakpoint, I can do `<br class="hidden lg:inline">` to break the paragraph when the page hits the large breakpoint

### Hover, focus, etc
* I use a similar pattern as with breakpoints (e.g. `hover:bg-blue`.)
* In order for state changes to have an effect, I have to include the `transform` class
* Include the `transition` class to smooth out the changes
* I have to configure Tailwind to work on `active` state in `tailwind.config.js`. It looks like this is not necessary in Tailwind 3
* I can also do things like `md:hover:bg-grey`
* If something does not work, it is likely that I have to set them up in the config file

### Misc
* There is a `print` variant to set things for printing
* Aspect ratio preserving classes
* Snapping scroll: I can set it up, so that when scrolling, an element will snap at some point, making scrolling sticky. Maybe useful for making slides
* Add smooth scrolling when using inner links
* `scroll-mt-#`: When scrolling to an inner link, it can scroll past a number (`#`). Useful when I have a navbar and I want the scrolled section to go right below it
* Use column classes from Tailwind
* I can set the columns so that the browser decides how many columns to make for a set column width
* Classes for form (and child) elements (some classes for file upload elements)
* The `<details>` tag has an `open` attribute that can be toggled and styled (with `open:`). The way it works is that if `open` is not present, only the `<summary>` tag inside `<details>` will be shown. If `open` is present, I a child element (e.g. `<div>`) will be shown
* Cool decoration for links
* I can use CSS properties not included in Tailwind, but with Tailwind variants (e.g. `hover:`), by enclosing the property in `[ ]`: `lg:[property_in_css]`. If the CSS property includes spaces, replace them with `_`