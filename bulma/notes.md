# Bulma
* hero class is like a header?
* I can also start with `section > container` elements
* There is a cool debugger stylesheet that has margins, and colors for all the elements
* Add columns as needed. Put `columns` in the container node, and `column` for each child
* Responsive
* modifiers can change text/background, etc
* I can add `is-one-third` to make columns one third of the width
* I can use Bulma with CSS grid
* Bulma adds padding after each section class
* There are classes like "notification", "box", "card", to make boxes with backgrounds
* By default, Bulma makes elements responsive, but I can fix a layout with the modifier `is-mobile`
* With the class `level`, Bulma treats all the elements in it as part of the same level. Also put `level-item` for the elements of a level
* I can remove margins and padding with `is-marginless` and `is-paddingless`
* The `content` class styles elements inside it. I dont have to set classes for titles, paragraphs, etc. I can add `is-small`, etc
* To use different images in mobile, use css background and media queries
* **navbar**: there are classes to separate elements of a navbar
* `is-overlay` makes an element's position independent of the page and ignores the position of other elements

That was the end of the tutorial. It was not very helpful as a tutorial, but it gave a broad overview of some classes that Bulma offers. It is better to just look a the [documentation](https://bulma.io/documentation/), and go through the sections there