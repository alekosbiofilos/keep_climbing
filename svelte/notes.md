# Svelte

* Components are `.svelte` files with their own css, html and js
* in the `<script>` tag, I can add data to be displayed in the component
```svelte
<script>
	let name = 'juan';
</script>

<h1>Hello {name.toUpperCase()}!</h1>
```
* In the html part, I can put any js inside curly braces
* If I am using an attribute from the js data, if it is named the same as the attribute, I can do this:
```svelte
<script>
	let src = 'tutorial/image.gif';
	let name = 'Rick Astley';
</script>

<img {src} alt="{name} dances.">
```
* The `<style>` part of a component, is scoped only to that component
* I can import components in files as:
```svelte
<script>
	import Nested from './Nested.svelte';
</script>

<p>This is a paragraph.</p>
<Nested/>

<style>
	p {
		color: purple;
		font-family: 'Comic Sans MS', cursive;
		font-size: 2em;
	}
</style>
```
* In the example above, even though the imported component is imported, the css does not leak into it. It maintains its own style
* If the data contains html code, I can render it as:
```svelte
<script>
	let string = `this string contains some <strong>HTML!!!</strong>`;
</script>

<p>{@html string}</p>
```

## Reactivity
* Svelte can render components when the underlaying data has changed
```svelte
<script>
	let count = 0;

	function incrementCount() {
		count += 1;
	}
</script>

<button on:click={incrementCount}>
	Clicked {count} {count === 1 ? 'time' : 'times'}
</button>
```
* Here, I just attach a js function to a `on:` event
* To tell Svelte to update a part of a component when other parts have changed, use reactive declarations with $
```svelte
<script>
	let count = 0;
	$: doubled = count * 2;

	function handleClick() {
		count += 1;
	}
</script>

<button on:click={handleClick}>
	Clicked {count} {count === 1 ? 'time' : 'times'}
</button>
```
* Here, `doubled` will be updated whenever `count` changes
* I can also group reactive declarations in a group
```svelte
$: {
	console.log(`the count is ${count}`);
	alert(`I SAID THE COUNT IS ${count}`);
}
```
* I can also set an if statement as reactive, so it will run every time a value changes
```svelte
$: if (count >= 10) {
	alert(`count is dangerously high!`);
	count = 9;
}
```
* Svelte reactivity is triggered by assignments. So array methods like push dont work. For example to add an element to an array, I have to do:
```svelte
function addNumber() {
	numbers[numbers.length] = numbers.length + 1;
}
```
* Note: the name of the updated variable must appear on the left hand side, so if I have 
```svelte
const foo = obj.foo;
foo.bar = 'baz';
```
* Svelte wont react, I have to put `obj = obj` afterwards to trigger it
## Passing data between components
* In order to pass data between components, use props
```svelte
<!-- Nested.svelte -->
<script>
	export let answer;
</script>
```
* And in the component that imports:
```svelte
<script>
	import Nested from './Nested.svelte';
</script>

<Nested answer={42}/>
```
* This will pass 42 to the `answer` variable in `<Nested>`
* If I set a value on the value to be passed, it behaves like a default value
```svelte
<script>
	export let answer = 'a mystery';
</script>
```
* An object of props can be passed as an object, instead of each property. For example, given the js
```svelte
<script>
	import Info from './Info.svelte';

	const pkg = {
		name: 'svelte',
		version: 3,
		speed: 'blazing',
		website: 'https://svelte.dev'
	};
</script>
``
* Instead of `<Info name={pkg.name} version={pkg.version} speed={pkg.speed} website={pkg.website}/>`
* I can do `<Info {...pkg}/>`
## Control statements
* If structure
```svelte
{#if user.loggedIn}
	<button on:click={toggle}>
		Log out
	</button>
{:else}
	<button on:click={toggle}>
		Log in
	</button>
{/if}
```
* In general *#* indicates block opening, and */* indicates block closing. *:* is continuation (for `else`)
* Loop over arrays
```svelte
<ul>
	{#each cats as cat}
		<li><a target="_blank" href="https://www.youtube.com/watch?v={cat.id}">
			{cat.name}
		</a></li>
	{/each}
</ul>
```
* I can get the index of each item:
```svelte
{#each cats as cat, i}
	<li><a target="_blank" href="https://www.youtube.com/watch?v={cat.id}">
		{i + 1}: {cat.name}
	</a></li>
{/each}
```
* When working with removing things from arrays, it is better to keep the key, so that Svelte knows which DOM element to remove:
```svelte
{#each things as thing (thing.id)}
	<Thing name={thing.name}/>
{/each}
```
## With async data
* I can await the result of a promise in the html part:
```svelte
{#await promise}
	<p>...waiting</p>
{:then number}
	<p>The number is {number}</p>
{:catch error}
	<p style="color: red">{error.message}</p>
{/await}
```
## Events
* Svelte can listen to any event on an element. This will display the coordinates of the mouse in real time
```svelte
<script>
	let m = { x: 0, y: 0 };

	function handleMousemove(event) {
		m.x = event.clientX;
		m.y = event.clientY;
	}
</script>

<div on:mousemove={handleMousemove}>
	The mouse position is {m.x} x {m.y}
</div>
```
* Declare event handlers inline
```svelte
<div on:mousemove="{e => m = { x: e.clientX, y: e.clientY }}">
	The mouse position is {m.x} x {m.y}
</div>
```
* Event handlers can have modifiers that alter their behavior. For example, "once" to run it only one time
```svelte
<script>
	function handleClick() {
		alert('no more alerts')
	}
</script>

<button on:click|once={handleClick}>
	Click me
</button>
```

* Components can dispatch events by using an event dispatcher
```svelte
<!-- Inner.svelte -->
<script>
	import { createEventDispatcher } from 'svelte';

	// this has to be called when the component is first instantiated
	const dispatch = createEventDispatcher();

	function sayHello() {
		// name of the event to be dispatched is "message"
		dispatch('message', {
			text: 'Hello!'
		});
	}
</script>

<button on:click={sayHello}>
	Click to say hello
</button>

<!-- App.svelte -->
<script>
	import Inner from './Inner.svelte';

	function handleMessage(event) {
		// .text is the attribute name from Inner.svelte
		alert(event.detail.text);
	}
</script>
<!-- This makes the app listen to the messages dispatched by Inner -->
<Inner on:message={handleMessage}/>
```
* Forwarding events from nested components: Say I have `App.svelte` that contains `Outer.svelte`, and `Outer.svelte` contains `Inner.svelte`. In order for `App.svelte` to listen to the "message" event from `Inner.svelte`:
```svelte
<!-- Inner.svelte -->
<script>
	import { createEventDispatcher } from 'svelte';

	const dispatch = createEventDispatcher();

	function sayHello() {
		dispatch('message', {
			text: 'Hello!'
		});
	}
</script>

<button on:click={sayHello}>
	Click to say hello
</button>
<!-- Outer.svelte -->
<script>
	import Inner from './Inner.svelte';
</script>

<Inner on:message/>
<!-- App.svelte -->
<script>
	import Outer from './Outer.svelte';

	function handleMessage(event) {
		alert(event.detail.text);
	}
</script>

<Outer on:message={handleMessage}/>
```
* Event forwarding also works for DOM events. To listen to a click, I can just do `on:click={handleClick}`

## Bindings
Data flows from parent component to child component: a parent component can set props on child components, but not the other way around. In order to make parent components to react to child/siblings data:
```svelte
<script>
	let name = 'world';
</script>
<!-- With this, "value" and "name are bound, so when "value" changes, "name" changes as well (and vice versa)-->
<input bind:value={name}>

<h1>Hello {name}!</h1>
```
* I can coherce variables to be numeric by binding them to variables that are:
```svelte
<script>
	// here I can coherce numeric variables
	let a = 1;
	let b = 2;
</script>

<label>
	<input type=number bind:value={a} min=0 max=10>
	<input type=range bind:value={a} min=0 max=10>
</label>

<label>
	<input type=number bind:value={b} min=0 max=10>
	<input type=range bind:value={b} min=0 max=10>
</label>

<p>{a} + {b} = {a + b}</p>

<style>
	label { display: flex }
	input, p { margin: 6px }
</style>
```
* Here is how I should bind a checkbox

```svelte
<script>
	let yes = false;
</script>

<label>
	<input type=checkbox bind:checked={yes}>
	Yes! Send me regular email spam
</label>
```
* I have multiple inputs relating to the same value (like a menu, or an array), I can use `bind:group`.
```svelte
<script>
	let scoops = 1;
	let flavours = ['Mint choc chip'];

	let menu = [
		'Cookies and cream',
		'Mint choc chip',
		'Raspberry ripple'
	];
</script>
<!-- This will make the selected option bound to the "scoops" variable -->
<label>
	<input type=radio bind:group={scoops} name="scoops" value={1}>
	One scoop
</label>

<label>
	<input type=radio bind:group={scoops} name="scoops" value={2}>
	Two scoops
</label>

<label>
	<input type=radio bind:group={scoops} name="scoops" value={3}>
	Three scoops
</label>

<!-- and this will put all checked values in the array "flavours" -->
{#each menu as flavour}
	<label>
		<input type=checkbox bind:group={flavours} name="flavours" value={flavour}>
		{flavour}
	</label>
{/each}
```
* If the names match, I dont have to put them twice. So `<textarea bind:value={value}></textarea>` is the same as `<textarea bind:value></textarea>`
* With `<select>` elements, I put the binding there: `<select bind:value={selected} on:change="{() => answer = ''}">`
* If `<select>` can have multiple attributes, an array will be created
```svelte
<select multiple bind:value={flavours}>
	{#each menu as flavour}
		<option value={flavour}>
			{flavour}
		</option>
	{/each}
</select>
```
* I can bind things to the "innerHTML" attribute of elements: `<div contenteditable="true" bind:innerHTML={html}></div>`. Here, whatever is in the contents of the div, will be bound to the "html" variable
* I can bind time, duration, paused, etc to audio/video elements, to build custom controls
* I can bind a rendered component to a variable for reference. The following will put the element "canvas" in a variable, so I can manipulate the element/component with js
```svelte
<canvas
	bind:this={canvas}
	width={32}
	height={32}
></canvas>
```
* I can also bind to props of other components

## Lifecycle
* Every component has a lifecycle that starts when the component is created, and ends when it is destroyed.
* `onMount` is a function that can run things when a component is first rendered to the DOM
* It is good practice to fetch data with `onMount`, so that it is not run with server-side rendering (SSR), but lazily when the element is being loaded
* Lifecycle functions must be called while the component is initializing
* if `onMount` returns a function, it will be called when the component is destroyed
```svelte
<script>
	import { onMount } from 'svelte';

	let photos = [];
	// this will load data when the component is rendered
	onMount(async () => {
		const res = await fetch(`https://jsonplaceholder.typicode.com/photos?_limit=20`);
		photos = await res.json();
	});
</script>

<h1>Photo album</h1>

<div class="photos">
	{#each photos as photo}
		<figure>
			<img src={photo.thumbnailUrl} alt={photo.title}>
			<figcaption>{photo.title}</figcaption>
		</figure>
	{:else}
		<!-- this block renders when photos.length === 0 -->
		<p>loading...</p>
	{/each}
</div>

```
* `onDestroy` is important, because cleaning it up when it is not relevant can prevent memory leaks
* `beforeUpdate` runs work right before the DOM is updated. Similarly for `afterUpdate`
* `tick` runs either after any pending changes have been applied to the DOM, or right away if there are no pending changes

## Stores
A store is an object that other things can subscribe to (using a `subscribe` method), so that they get notified when the store value changes
### Writable stores
This is a `writable` store. It has an `update` and `set` methods
```js
// stores.js
import { writable } from 'svelte/store';

export const count = writable(0);
```
* Now, I can subscribe to that store:
```svelte
<script>
	import { count } from './stores.js';

	let count_value;

	count.subscribe(value => {
		count_value = value;
	});
</script>
```
* and use it to update it
```svelte
<script>
	import { count } from './stores.js';

	function increment() {
		count.update(n => n + 1)
	}
</script>
```
* Using this approach, I have to unsubscribe to the store when I am done with it (e.g. with `onDestroy`). When I call a `subscribe` method, an `unsubscribe` function is returned
```svelte
<script>
	import { onDestroy } from 'svelte';
	import { count } from './stores.js';

	let count_value;

	const unsubscribe = count.subscribe(value => {
		count_value = value;
	});

	onDestroy(unsubscribe);
</script>
```
* I can also auto-subscribe to a store, and reference it by value with `$count` either in HTML or script

### Read-only stores
* It has two arguments
	* The first argument is an initial value (it can be null)
	* The second argument is a `start()` function that gets called when the store gets its first subscriber, and it returns a `stop()` function that runs when the last subscriber unsubscribes
```js
export const time = readable(new Date(), function start(set) {
	const interval = setInterval(() => {
		set(new Date());
	}, 1000);

	return function stop() {
		clearInterval(interval);
	};
});
```
### Derived stores
I can create a store whose value depends on the value of other stores. For example, here the store "elapsed" gets its value from the store "time"
```js
export const elapsed = derived(
	time,
	$time => Math.round(($time - start) / 1000)
);
```

### Custom stores
As long as a store implements a `subscribe` method, it is a store. With that, I can create an object store with more functionality. Here, a store is created, and in addition to the writable methods `subscribe`, `set`, and `update`
```js
import { writable } from 'svelte/store';

function createCount() {
	const { subscribe, set, update } = writable(0);

	return {
		subscribe,
		increment: () => update(n => n + 1),
		decrement: () => update(n => n - 1),
		reset: () => set(0)
	};
}

export const count = createCount();
```
### Bind to stores
If a store is writable, I can bind values to stores. Here, the `greeting` store is derived from the `name` store, and I can bind it to the input and button
```svelte
<!-- stores.js -->
import { writable, derived } from 'svelte/store';

export const name = writable('world');

export const greeting = derived(
	name,
	$name => `Hello ${$name}!`
);
```
```svelte
<!-- App.svelte -->
<script>
	import { name, greeting } from './stores.js';
</script>

<h1>{$greeting}</h1>
<input bind:value={$name}>

<button on:click="{() => $name += '!'}">
	Add exclamation mark!
</button>

## Motion
### Tweening
Tweening is a way of communicating changes in the data using motion. I can replace writeble stores with it (?)
* Here, I can animate how a bar changes its value. There are more options...
```svelte
<script>
	import { tweened } from 'svelte/motion';
	import { cubicOut } from 'svelte/easing';

	const progress = tweened(0, {
		duration: 400,
		easing: cubicOut
	});
</script>
```
* `spring` is an alternative to `tweening` that adds some springiness to the motion

## Transitions
I can easily make transitions when elements get in or out of the DOM
```svelte
<script>
	import { fade } from 'svelte/transition';
	let visible = true;
</script>

<label>
	<input type="checkbox" bind:checked={visible}>
	visible
</label>

{#if visible}
	<p transition:fade>
		Fades in and out
	</p>
{/if}
```
* There are other transitions and effects
* Transitions are reversible.
* Instead of using the same transition for both in and out, I can specify what happens when the element comes in/goes out of the DOM
```svelte
<script>
	import { fade, fly } from 'svelte/transition';
	let visible = true;
</script>

<label>
	<input type="checkbox" bind:checked={visible}>
	visible
</label>

{#if visible}
	<p in:fly="{{ y: 200, duration: 2000 }}" out:fade>
		Flies in, fades out
	</p>
{/if}
```
### Custom CSS transitions
I can make a transition by specifying the changes to the CSS that have to be made to the element. For example, this is the source for the `fade` transition. The function takes two arguments: the element to be transitioned, and a transition object, that can have some properties. The `t` values is 0 at the beginning of the intro, and 1 at the end of the outro
```js
function fade(node, {
	delay = 0,
	duration = 400
}) {
	const o = +getComputedStyle(node).opacity;

	return {
		delay,
		duration,
		css: t => `opacity: ${t * o}`
	};
}
```

### custom JS transitions
CSS transitions are preferred. However, there are some transitions that have to be done with JS. In this example, the typewriter effect
```svelte
<script>
	let visible = false;

	function typewriter(node, { speed = 1 }) {
		const valid = (
			node.childNodes.length === 1 &&
			node.childNodes[0].nodeType === Node.TEXT_NODE
		);

		if (!valid) {
			throw new Error(`This transition only works on elements with a single text node child`);
		}

		const text = node.textContent;
		const duration = text.length / (speed * 0.1);

		return {
			duration,
			// t is the step, so in this function, it calculates the length of the
			// string to be displayed at each step
			tick: t => {
				const i = Math.trunc(text.length * t);
				node.textContent = text.slice(0, i);
			}
		};
	}
</script>

<label>
	<input type="checkbox" bind:checked={visible}>
	visible
</label>

{#if visible}
	<p transition:typewriter>
		The quick brown fox jumps over the lazy dog
	</p>
{/if}
```
### Transition events
Svelte can dispatch events when transitions start/end. It is useful if I want to do something when a transition starts/ends. In this example, I have functions attatched to the variable `status` that trigger when different transitions start and end
```svelte
<p>status: {status}</p>
<p
	transition:fly="{{ y: 200, duration: 2000 }}"
	on:introstart="{() => status = 'intro started'}"
	on:outrostart="{() => status = 'outro started'}"
	on:introend="{() => status = 'intro ended'}"
	on:outroend="{() => status = 'outro ended'}"
>
	Flies in and out
</p>
```
### Local transitions
Usually, transitions play whenever any container block is added/destroyed. If I want a transition to play only when individual items are added/destroyed, I use a `local` transition. Here, he slide transition will play only when the block with the transition itself is added or removed. If I dont use `local`, the slide transition will play when the div element is created. However, with `local`, the transition will only play when elements inside the `<div>` are created/destroyed.
```svelte
<script>
	import { slide } from 'svelte/transition';

	let showItems = true;
	let i = 5;
	let items = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];
</script>

<label>
	<input type="checkbox" bind:checked={showItems}>
	show list
</label>

<label>
	<input type="range" bind:value={i} max=10>

</label>

{#if showItems}
	{#each items.slice(0, i) as item}
		<div transition:slide|local>
			{item}
		</div>
	{/each}
{/if}

```
### Deferred transitions
This is used to coordinate several transitions. For example, an object dissapearing in one side and reappearing in another place can be coordinated, so that the object moves from one place to the other. An example is the `crossfade` function. It creates a pair of transitions (`send` and `receive`). When an element is sent, it looks for a corresponding element being received elsewhere, and coordinates intermediate transitions between the two. An example can be seen [here](https://svelte.dev/tutorial/deferred-transitions)

### Key blocks
This can be used when I want a transition to occur when a value changes. Here, the transition `fade` will trigger when `value` changes
```svelte
{#key value}
	<div transition:fade>{value}</div>
{/key}
```

## Animations
Just by using `animate:flip`, I can make objects move from/to position instead of appearing/dissapearing
```svelte
<script>
	import { flip } from 'svelte/animate';
</script>
<label
	in:receive="{{key: todo.id}}"
	out:send="{{key: todo.id}}"
	animate:flip
>
```

## Actions
Actions are "element-level lifecycle functions"(?). Actions receive a node to be applied to, and returns an action object (I dont get it. Ref [here](https://svelte.dev/tutorial/actions))

## Classes
* I can add  class if a condition is met. Here, if `foo` is true, I will add the class `selected` to the button
```svelte
<button
	class:selected="{current === 'foo'}"
	on:click="{() => current = 'foo'}"
>foo</button>
```
* If the name of the class is the same as the name of the value it depends on, there is a shorthand
```svelte
<!-- this -->
<div class:big={big}>
	<!-- ... -->
</div>
<!-- is the same as this -->
<div class:big>
	<!-- ... -->
</div>
```
## Component composition
I can put children inside components. The only thing I need to do, is to add `<slot>` where the children are going to be placed. In this example, the children I put on `<Box>`, are going to be displayed between "something before" and "something after". Furthermore, if no children are inserted, the block inside `<slot>` will be rendered
```svelte
<!-- in Box.svelte -->
<div>
	<p>
		something before
	</p>
	<slot>
	<em>no children</em>
	</slot>
	<p>
		something after
	</p>
</div>
```
```svelte
<!-- In App.svelte -->
<Box>
	<h2>Hello!</h2>
	<p>This is a box. It can contain anything.</p>
</Box>
```
I can actually have more than one slot per component. In order to set in which slot a set of children are going to be placed, I can name them
```svelte
<!-- ContactCard.svelte -->
 <article class="contact-card">
	<h2>
		<slot name="name">
			<span class="missing">Unknown name</span>
		</slot>
	</h2>

	<div class="address">
		<slot name="address">
			<span class="missing">Unknown address</span>
		</slot>
	</div>

	<div class="email">
		<slot name="email">
			<span class="missing">Unknown email</span>
		</slot>
	</div>
</article>
```
```svelte
<!-- in App.svelte -->
<script>
	import ContactCard from './ContactCard.svelte';
</script>

<ContactCard>
	<span slot="name">
		P. Sherman
	</span>

	<span slot="address">
		42 Wallaby Way<br>
		Sydney
	</span>
</ContactCard>
```

### Checking if slot has children
Within a component, the object `$$slots` have each named slot as property, and their value is true if they have children. For example `<article class:has-discussion={$$slots.comments}>` will add the class `has-discussion` to the article element, if the slot `comments` have children inside
### Slot props
I can pass props from the component definition (e.g. Component.svelte) in a slot, that are accessible to the component implementation (e.g. in App.svelte) by passing slot props. Here, whenever the `hovering` variable changes inside the slot/component, its state can be changed in App.svelte
```svelte
<!-- in Hoverable.svelte -->
<slot hovering={hovering}></slot>
<!-- in App.svelte -->
<Hoverable  let:hovering={hovering} >
	<div class:active={hovering}>
		{#if hovering}
			<p>I am being hovered upon.</p>
		{:else}
			<p>Hover over me!</p>
		{/if}
	</div>
</Hoverable>
```

## Context API
This provides a mechanisms for components to share information without having to send data around.  
* A context has two parts: `setContext(key, context)` and `getContext()`
* Both functions have to be called during component initialization. Basically, `setContext` returns an object with shared data, and `getContext` returns that object, so the component calling `getContext` can have access to the shared data
```svelte
<!-- set the context.  -->
import { onDestroy, setContext } from 'svelte';
import { mapbox, key } from './mapbox.js';

setContext(key, {
	getMap: () => map
});
```
```svelte
<!-- Use the context -->
import { getContext } from 'svelte';
import { mapbox, key } from './mapbox.js';

const { getMap } = getContext(key);
const map = getMap();
```
* Contexts differ from stores in that the data in contexts is only available to the context and its descendants. Also, context is not reactive

## Special Svelte elements

### `<svelte:self>`

This allows me to put one container into itself (e.g. a folder container within a folder container). This will create a `Folder` component within `Folder.svelte`
```svelte
<!-- to have a folder within a folder. In Folder.svelte -->
<svelte:self {...prop_obj}> 
``` 
### `<svelte:component>`
`svelte:component` can be used to change a component type. So instead of
```svelte
{#if selected.color === 'red'}
	<RedThing/>
{:else if selected.color === 'green'}
	<GreenThing/>
{:else if selected.color === 'blue'}
	<BlueThing/>
{/if}
```
I can do this
```svelte
	const options = [
		{ color: 'red',   component: RedThing   },
		{ color: 'green', component: GreenThing },
		{ color: 'blue',  component: BlueThing  },
	];
<svelte:component this={selected}/>
```
Where `selected` is one of the elements in `options`

### `<svelte:window>`
`svelte:window` can be used to add event listeners to the window itself. For example, `<svelte:window on:keydown={handleKeydown}/>` runs the `handleKeyDown` function whenever a key is pressed anywhere in the window
* I can also bind to window properties. For example `<svelte:window bind:scrollY={y}/>` will bind the number of pixels scrolled in Y, to the varible `y`. These are the properties that can be bound
	* `innerWidth`, `innerHeight`
	* `outerWidth`, `outerHeight`
	* `scrollX`, `scrollY`
	* `online`

### `<svelte:body>`
Similar to `svelte:window`, but to bind things/add event listeners to the body of the document
```svelte
<svelte:body
	on:mouseenter={handleMouseenter}
	on:mouseleave={handleMouseleave}
/>
```

### `<svelte:head>`
This is for document.head. For example, to set a different theme with a button
```svelte
<svelte:head>
	<link rel="stylesheet" href="/tutorial/dark-theme.css">
</svelte:head>
```

### `<svelte:options>`
With this element, I can set compiling options. For example, `<svelte:options immutable={true}/>` will make Svelte to assume that the data passed to the component will never change. this can be used to optimize the build. There is more info [here](https://svelte.dev/docs#compile-time-svelte-compile)

### `<svelte:fragment>`
This allows me to put tags inside a named slot, without wrapping them in a DOM element. Here, I am putting the two `<p>` elements inside the `footer` slot without any wrapping element. This is useful when I have CSS rules that will conflict if I have extra markup there.
```svelte
<svelte:fragment slot="footer">
	<p>All rights reserved.</p>
	<p>Copyright (c) 2019 Svelte Industries</p>
</svelte:fragment>
```

## Module context
For the most part, the `<script>` tag will be run on component initialization. Code inside `<script context="module">` will run once: when the module first evaluates, rather than when a component is initialized
* I can also export functions from `<script context="module">` so they are accessible from other parts of the app
```svelte
<!-- in a component -->
<script context="module">
	const elements = new Set();

	export function stopAll() {
		elements.forEach(element => {
			element.pause();
		});
	}
</script>
```
```svelte
<!-- in App.svelte -->
<script>
	import AudioPlayer, { stopAll } from './AudioPlayer.svelte';
</script>
<button on:click={stopAll}>
	stop all audio
</button>
```

## The debug tag
I can stop execution whenever a value changes, by including it in the `{@debug values}` tag. For example, every time `user` changes, I will be able to interact with it inside devtools
```svelte
{@debug user}

<h1>Hello {user.firstname}!</h1>
```