"use strict";
let hello = "hello";
// I cant do hello = 2, because it was set to string
// Functions
const getName = (name, surname) => {
    return `${name} ${surname}`;
};
console.log(getName("Juan", "Ortiz"));
const user1 = {
    username: "Juan",
    age: 38,
    getMessage(username) {
        return " Hello, " + username;
    }
};
// Union operator with types
let pageName = "1";
// For things that might be defined after, include null
let maybeNull = null;
// Set type of function that doesnt return anything
const doSomething = () => {
    console.log("do something");
};
// Convert between types
let s1 = 10;
let s2 = s1;
// from here, work with index.html
const someElement = document.querySelector(".foo");
someElement.addEventListener("blur", (event) => {
    const target = event.target;
    console.log(target.value);
});
// classes
class UserClass {
    constructor(first, last) {
        this.firstName = first;
        this.lastName = last;
    }
    getFullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}
// static properties are available in classes but not in their instances
UserClass.maxAge = 50;
// instantiate
const user = new UserClass("Juan", "Ortiz");
class Admin extends UserClass {
    constructor() {
        super(...arguments);
        // I can also make functions that get and set those properties
        this.reboot = true;
    }
}
const addID = (obj) => {
    const id = Math.random().toString();
    return Object.assign(Object.assign({}, obj), { id });
};
const me = {
    username: "juan",
    getMessage: (username) => { return "hello, " + username; }
};
// this object will be the same as User, but adding the id property
const withId = addID(me);
// with generics
const addID2 = (obj) => {
    const id = Math.random().toString();
    return Object.assign(Object.assign({}, obj), { id });
};
const me2 = {
    username: "juan",
    getMessage: (username) => { return "hello, " + username; }
};
// this object will be the same as User, but adding the id property
// this works
// const withId2 = addID2(me2)
//but this is more explicit
const withId2 = addID2(me2);
const yo = {
    nombre: "juan",
    tipo: "algo",
    otraCosa: "meh"
};
const otro = {
    nombre: "felipe",
    tipo: 10,
    otraCosa: true
};
// enums 
var Status;
(function (Status) {
    Status[Status["NotStarted"] = 0] = "NotStarted";
    Status[Status["InProgress"] = 1] = "InProgress";
    Status[Status["Done"] = 2] = "Done";
})(Status || (Status = {}));
// The values will be populated starting from zero
// I can also use strings as values
var StringStatus;
(function (StringStatus) {
    StringStatus["notStarted"] = "not started";
    StringStatus["done"] = "done";
})(StringStatus || (StringStatus = {}));
