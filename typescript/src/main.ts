let hello: string = "hello"
// I cant do hello = 2, because it was set to string


// Functions
const getName = (name: string, surname: string): string => {
    return `${name} ${surname}`
} 

console.log(getName("Juan", "Ortiz"))

// Interfaces
// Specify properties of objects

interface User {
    username: string,
    // the ? makes a property optional
    age?: number,
    // define methods for objects
    getMessage(username: string): string
}

const user1: User = {
    username: "Juan",
    age: 38,
    getMessage(username) {
        return " Hello, " + username
    }
}

// Union operator with types
let pageName: string | number = "1"
// For things that might be defined after, include null
let maybeNull: string | null = null

// this is a custom type that can be either string or a number
type ID = string | number

// Set type of function that doesnt return anything
const doSomething = (): void => {
    console.log("do something")
}
// Convert between types
let s1: number = 10
let s2: string = (s1 as unknown) as string

// from here, work with index.html

const someElement = document.querySelector(".foo") as HTMLInputElement

someElement.addEventListener("blur", (event) => {
    const target = event.target as HTMLInputElement
    console.log(target.value)
})

interface UserInterface {
    getFullName(): string
}

// classes
class UserClass implements UserInterface{
    // I have to put the undefined part, because the property has not been initialized yet
    firstName: string | undefined
    // however, since I am using a constructor, I dont have to
    private lastName: string
    // static properties are available in classes but not in their instances
    static maxAge = 50
    constructor(first: string, last: string) {
        this.firstName = first
        this.lastName = last
    }
    getFullName(): string {
        return `${this.firstName} ${this.lastName}`
    }
}



// instantiate
const user = new UserClass("Juan", "Ortiz")

class Admin extends UserClass {
    // I can also make functions that get and set those properties
    reboot: boolean = true
}

const addID = (obj: User) => {
    const id = Math.random().toString();
    return {
        ...obj,
        id
    }
}

const me: User = {
    username: "juan",
    getMessage: (username) =>{return "hello, " + username}
}

// this object will be the same as User, but adding the id property
const withId = addID(me)

// with generics
const addID2 = <T extends object>(obj: T) => {
    const id = Math.random().toString();
    return {
        ...obj,
        id
    }
}

const me2: User = {
    username: "juan",
    getMessage: (username) =>{return "hello, " + username}
}

// this object will be the same as User, but adding the id property
// this works
// const withId2 = addID2(me2)

//but this is more explicit
const withId2 = addID2<User>(me2)

// generics with interfaces
interface Usuario<T, V> {
    nombre: string
    tipo: T
    otraCosa: V
}

const yo: Usuario<string, string> = {
    nombre: "juan",
    tipo: "algo",
    otraCosa: "meh"
}

const otro: Usuario<number, boolean> = {
    nombre: "felipe",
    tipo: 10,
    otraCosa: true
}

// enums 
enum Status {
    NotStarted,
    InProgress,
    Done
}

// The values will be populated starting from zero

// I can also use strings as values
enum StringStatus {
    notStarted = "not started",
    done = "done"
}