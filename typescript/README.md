# TypeScript
I will be following the youtube tutorial from [freeCodeCamp.org](https://www.youtube.com/watch?v=gp5H0Vw39yw)
* command: `tsc`
* transpile to js: `tsv file.ts`, and a js file will be created wit the same name. By default, it transpiles to ES3 (watch mode with `tsc file.ts -w`)

## Configuration file
* `tsconfig.json`: complete reference can be found in the [documentation](https://www.typescriptlang.org/tsconfig)
* I can create `tsconfig` with `tsc --init`. Then, I can edit to specify options
* After having a `tsconfig.json`, I compile with `tsc`, no need to specify files or output

## Defining types
* With `const`, the type is the value itself, and it cant change at all.
* I can change values only of the same type
* TypeScript infers type from definition, but it is better to explicilty put it

## Fuctions
* Define types of parameters and return values
```typescript
const getName = (name: string, surname: string): string => {
    return `${name} ${surname}`
}
```

## Interfaces
* With interfaces, I can create objects with specific typed properties that then I can assign to other things
```typescript
interface User {
    username: string,
    // the ? makes a property optional
    age?: number,
    // define methods for objects
    getMessage(username: string): string
}

const user1: User = {
    username: "Juan",
    age: 38,
    getMessage(username) {
        return " Hello, " + username
    }
}
```

* Union operators allow me to define several types for a variable
```ts
let pageName: string | number = "1"
// For things that might be defined after, include null
let maybeNull: string | null = null
```

## Type aliases
* This might be useful to make the code consistent. If the type of an ID changes, I only need to change the type in the alias definition
```ts
type ID = string | number
```
## Arrays
* To define the type of the elements of an array: do `let arr: string[] = ["a", "b"]`

## Void type
* It is a good idea to set the return value `void` on a function that does not return anything
```ts
const doSomething = (): void => {
    console.log("do something")
}
```
## Type unknown
It is like `any`, but typescript does not ignore it (?)
## Type assertion
* I can cast a variable as another type with `as`. However, the variable to be casted has to be unknown (or any). If it is not, I have to convert it first
```ts
let s1: number = 10
let s2: string = (s1 as unknown) as string
```

## Working with the DOM
* TS does not know what element I am selecting when using classes or ids, so it assigns the type `Element`.
* So I have to specify the type of element, if I want specific attributes. Here, I tell TS that the element is an input object, so it makes input attributes available
* The trick is to use `as` correctly, to tell TS what kins of elements I am working with
```ts
const someElement = document.querySelector(".foo") as HTMLInputElement

someElement.addEventListener("blur", (event) => {
    const target = event.target as HTMLInputElement
    console.log(target.value)
})
```

## Classes
```ts
// with the interface and `implements` I can force a class to implement the methods of the interface it implements
interface UserInterface {
    getFullName(): string
}

// classes
class UserClass implements UserInterface{
    // I have to put the undefined part, because the property has not been initialized yet
    firstName: string | undefined
    // however, since I am using a constructor, I dont have to
    private lastName: string
    // static properties are available in classes but not in their instances
    static maxAge = 50
    constructor(first: string, last: string) {
        this.firstName = first
        this.lastName = last
    }
    getFullName(): string {
        return `${this.firstName} ${this.lastName}`
    }
}

// instantiate
const user = new UserClass("Juan", "Ortiz")
```
* Protected: like private, but it will be available in inherited classes (?)
* readonly: property that cant be changed

### Inheritance
* I can make classes that inherit from other classes
```ts
class Admin extends UserClass {
    // I can also make functions that get and set those properties
    reboot: boolean = true
}
```
### How to add properties
```ts
const addID = (obj: User) => {
    const id = Math.random().toString();
    return {
        ...obj,
        id
    }
}

const me: User = {
    username: "juan",
    getMessage: (username) =>{return "hello, " + username}
}

// this object will be the same as User, but adding the id property
const withId = addID(me)
```
## Generics
In the snipper above, I specified the User type. I can also use a generic type `<T>`, where T is the type that I assigned. However, I think it is better to define an interface and pass it as the type
```ts
const addID2 = <T>(obj: T) => {
    const id = Math.random().toString();
    return {
        ...obj,
        id
    }
}

const me2: User = {
    username: "juan",
    getMessage: (username) =>{return "hello, " + username}
}

// this object will be the same as User, but adding the id property
// this works
// const withId2 = addID2(me2)

//but this is more explicit
const withId2 = addID2<User>(me2)
```
* However, with the implementation above, I could pass anything to `addID2()`. In order to specify that the type must contain an object, I should implement addID2 as:
```ts
const addID2 = <T extends object>(obj: T) => {
    const id = Math.random().toString();
    return {
        ...obj,
        id
    }
}
```
### Generics with interfaces
* I can make a datatype optional, so the user specify the type when using an interface. I can pass one or multiple types
```ts
interface Usuario<T, V> {
    nombre: string
    tipo: T
    otraCosa: V
}

const yo: Usuario<string, string> = {
    nombre: "juan",
    tipo: "algo",
    otraCosa: "meh"
}

const otro: Usuario<number, boolean> = {
    nombre: "felipe",
    tipo: 10,
    otraCosa: true
}
```

## Enums
```ts
enum Status {
    NotStarted,
    InProgress,
    Done
}
```
* I can use enums as types, as in
```ts
const nonStarted: Status = Status.NotStarted
```
* Maybe useful to code scores, or scales, or things like that. However, I can only assign the values defined in the enum
```ts
let hasStarted: Status = Status.NotStarted
// this wont work
// hasStarted = 3
// this will work
hasStarted = Status.Done
```
* I can also setup enums with string values
```ts
enum StringStatus {
    notStarted = "not started",
    done = "done"
}
```
* I can also can use enums inside interfaces