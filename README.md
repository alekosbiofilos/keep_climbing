# Keep climbing

This is where I record my study sessions. From web development, to data visualization, to computational biology

## Current topics

### Bulma
Bulma is a CSS framework. I am checking it out as an alternative to Bootstrap. Hopefully it is easier to customize. I am following the scrimba course from their [website](https://scrimba.com/learn/bulma). This tutorial was not great, and Bulma does not seem to bring anything too exciting in my opinion

### Django
This is my backend of choice. Here I followed the three Django books by [William Vincent](https://github.com/wsvincent): [Django for beginners](https://djangoforbeginners.com/), [Django for professionals](https://djangoforprofessionals.com/), and [Django for APIs](https://djangoforapis.com/)

### Docker
Docker provides a lot of cool features for building developing and deploying environments. I followed and excelent [tutorial](https://www.youtube.com/watch?v=3c-iBn73dDE&t=169s) by [Techworld with Nana](https://www.techworld-with-nana.com/)

### Javascript
To learn Javascript, I went to the basic [tutorial on Scrimba](https://scrimba.com/learn/learnjavascript), and the Traversy media [YouTube tutorial](https://www.youtube.com/watch?v=PoRJizFvM7s) on async functions

### Nextflow
Nextflow is becoming a standard on developing bioninformatics pipelines. In order to get a primer, I usedthe official [Nextflow documentation](https://www.nextflow.io/docs/latest/getstarted.html). Since the docs incude every little detail, the notes I took contain a lot of references to links, so I could avoid just copy/paste

### Svelte
I am primarily a back-end person (Django). Thing is, the template system of Django is quite limited, so I am looking for alternatives to dive a bit deeper into front-end framework. I heard good things about Svelte, so i am checking it out. I am following their official [tutorial](https://svelte.dev/tutorial)

### Tailwind
This is the CSS framework I will be working with. I followed the [tutorial](https://www.youtube.com/watch?v=pfaSUYaSgRo) from Fireship.io, and supplemented it with videos from the [Tailwind labs](https://www.youtube.com/channel/UCOe-8z68tgw9ioqVvYM4ddQ) YouTube channel

### Typescript
To enforce easier-to-maintain code, I will try to use Typescript as much as I can. I learned the basics from [freeCodeCamp.org](https://www.youtube.com/watch?v=gp5H0Vw39yw)

### HTMX
I found this really cool library/framework/thingy called HTMX. It allows me to make reactive pages using only HTML. It is very useful because I can use Django HTML responses. This is the website of the [HTMX project](https://htmx.org/), and a cool YouTube [tutorial](https://www.youtube.com/playlist?list=PL-2EBeDYMIbRByZ8GXhcnQSuv2dog4JxY)
